#include <algorithm>

#include "Boy.hpp"

Boy::Boy(const std::string &_name) : name(_name) {}

const std::string &Boy::getName() const { return name; }

bool Boy::canBeWith(const std::list<const Person *> &persons,
                    std::string &error) const {
  std::list<const Person *>::const_iterator mother =
      find_if(persons.begin(), persons.end(),
              [](const Person *const person) { return person->isMother(); });

  if (mother != persons.end()) {
    std::list<const Person *>::const_iterator father =
        find_if(persons.begin(), persons.end(),
                [](const Person *const person) { return person->isFather(); });

    if (father == persons.end()) {
      error = "### Une garcon ne peut pas etre avec sa mere sans son pere";
      return false;
    }
  }

  // On pourrais juste retourner true mais on retourne la methode parent pour
  // rester compatible en cas d'evolution
  return Person::canBeWith(persons, error);
}

bool Boy::canDriveBoat() const { return false; }
