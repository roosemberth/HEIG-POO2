#include "Policeman.hpp"

using namespace std;

const string policemanName = "policier";

const string &Policeman::getName() const { return policemanName; }

bool Policeman::canDriveBoat() const { return true; }

bool Policeman::canStopThief() const { return true; }