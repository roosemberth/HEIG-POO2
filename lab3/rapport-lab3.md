---
title: "Rapport du laboratoire 3: Rivière"
lang: fr
author:
  - Florian Gazzetta
  - Roosemberth Palacios
---

## Schéma UML

```plantuml
@startuml
skinparam monochrome true
skinparam linetype polyline
skinparam linetype ortho
skinparam classAttributeIconSize 0
hide empty members
hide circle

abstract class Container {
  -persons : List<Person>
  +canAdd(Person p, String &error) : bool
  +canRemove(Person p, String &error) : bool
  +add(Person p) : void
  +remove(Person p) : void
  +empty() : void
  +getPersons() : List<Person>
}

abstract class Person {
  -name : String
  --Capabilities--
  +canDriveBoat() : bool
  +canStopThief() : bool
  +isMother() : bool
  +isFather() : bool
  +canBeWith(List<Person> people, string &err) : bool
}

class Father
class Mother
class Boy
class Girl
class Policeman
class Thief

Person <|-d- Father
Person <|-d- Mother
Person <|-d- Boy
Person <|-d- Girl
Person <|-d- Policeman
Person <|-d- Thief

class Controller {
  - tour : int
  - endOfTheGame : bool
  --operations--
  - load(String person) : void
  - unload(String person) : void
  - moveBoat() : bool
  - reinitialise() : void
  - printBoat() : void
  - boat : Boat
  - left : Bank
  - right : Bank
  --prompt management--
  + showMenu() : void
  + displayState() : void
  + nextCommand() : void
  --lifecycle management--
  + gameIsOver() : void
}

class Boat
class Bank

' Inheritance relations
Boat -u-|> Container
Bank -u-|> Container

Boat "0..1" -l-> "1" Bank : current >

Container "1" --> "*" Person : contains >
Controller "1" -u-> "*" Person : handles >

Controller "1" -l-> "2" Bank : handles >
Controller "1" -l-> "1" Boat : handles >
@enduml
```

## Détails d'implémentation

Dans l'implémentation que nous avons faite, la classe Controller s'occuppe de gérer la partie.
La méthode main peut interagir avec la classe controller a travers 4 méthodes :

- `showMenu` permet d'afficher le menu en debut de partie
- `display` affiche l'etat actuel de la partie
- `nextCommand` demande de lire la prochaine commande et de l'executer
- `gameIsOver` indique si la partie est terminée

La classe controller contient trois containers, qui contiennent des personnes.
Les containers fournissent des méthodes pour ajouter ou retirer des personnes,
mais aussi pour connaitre toutes les personnes qui sont contenues dans le containers
et pour vider le container.

En plus de cela, le container fournit deux méthodes pour savoir si on peut ajouter ou enlever une personne.
Chaque méthode utilise une référence vers une string pour pouvoir retourner un message d'erreur si l'opération
échoue. De plus, la méthode tryRemove retourne demande le nom de la personne a supprimer et pas un pointeur sur
cette dernière et retourne un pointeur sur la personne dans le cas ou elle peut être supprimée.
Concernant les personnes, elle disposent de différentes méthodes constantes qui retournent un booléena afin d'avoir
des informations a leur propos. En plus de cela, elle fournissent aussi une méthode canBeWith qui indiquent si elle
peuvent être inclues dans un autre groupe de personne ou non, et retourner un message d'erreur dans le cas ou elle
ne peuvent pas être inclues dans ce groupe.

Enfin, concernant la gestion de la mémoire, les personnes sont allouées dynamiquement et sont utilisées
par pointeur dans toutes l'application. La méthode qui vide un conteneur s'occupe de supprimer les
personne, mais dans les autres cas il de la responsabilité du devellepeur de s'assurer qu'un pointeur 
n'est jamais dupliqué ou perdu.

## Tests

- Nous avons fait des tests manuels.
- Nous avons effectué des tests avec _Valgrind_ pour vérifier qu'il n'y a pas
  de fuites de mémoire.
