#include "Person.hpp"

Person::~Person() {}

bool Person::canStopThief() const { return false; }

bool Person::isMother() const { return false; }

bool Person::isFather() const { return false; }

bool Person::canBeWith(const std::list<const Person *> &persons,
                       std::string &error) const {
  return true;
}
