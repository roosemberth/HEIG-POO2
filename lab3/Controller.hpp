#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#include "Bank.hpp"
#include "Boat.hpp"

class Controller {
public:
  Controller();
  ~Controller();

  /**
   * @brief Prints the menu of available options to stdout.
   */
  void showMenu() const;

  /**
   * @brief Displays the current game state.
   */
  void display() const;

  /**
   * @brief Ask the user for a command, retry until the command is valid.
   *
   * Will display a prompt to the user, if the action is not valid or would not
   * respect the rules, displays a message to the user and a new prompt.
   *
   * This method returns once the user has provided a valid input.
   */
  void nextCommand();

  /**
   * @brief returns whether the game is over.
   */
  bool gameIsOver() const;

private:
  /**
   * @brief Move the person from ashore to the boat.
   */
  bool load(const std::string &personName);

  /**
   * @brief Move the person from the Boat ashore.
   */
  bool unload(const std::string &personName);

  /**
   * @brief Moves the boat to the opposite shore.
   */
  bool moveBoat();

  /**
   * @brief Resets the Game back to the beginning.
   */
  void reinitialise();

  /**
   * @brief Prints a line with the state of the boat.
   */
  void printBoat() const;

  /**
   * @brief The boat.
   */
  Boat boat;

  /**
   * @brief Each shore.
   */
  Bank left, right;

  /**
   * @brief Counts how many times the user has provided a command.
   */
  int tour;

  /**
   * @brief Whether the game is finished.
   */
  bool endOfTheGame;
};

#endif // CONTROLLER_HPP
