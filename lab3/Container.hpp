#ifndef CONTAINER_HPP
#define CONTAINER_HPP

#include "Person.hpp"
#include <list>
#include <string>

class Container {
public:
  /**
   * @brief Whether the specified person could be added to this container.
   *
   * @returns Whether the operation would succeeded.
   *          If not, the error string will be populated with an adequate error
   *          message.
   */
  virtual bool canAdd(const Person *p, std::string &error);

  /**
   * @brief Whether the specified person could be removed from this container.
   *
   * If the person is not part of the container, this method returns true.
   *
   * @returns Whether the operation succeeded.
   *          If not, the error string will be populated with an adequate error
   *          message.
   */
  bool canRemove(const std::string &personName, const Person *&p,
                 std::string &error);

  /**
   * @brief Adds the specified person to this container.
   *
   * @throw std::runtime_exception if requested operation would violate any
   *        representation invariant.
   */
  void add(const Person *p);

  /**
   * @brief Removes the specified person from this container.
   *
   * @throw std::runtime_exception if the requested operation would violate any
   *        representation invariant.
   */
  void remove(const Person *p);

  /**
   * @brief Remove all persons in this container
   */
  void empty();

  /**
   * @brief Return a list of all the persons in the container
   * @return A list of all the persons in the container
   */
  const std::list<const Person *> &getPersons() const;

protected:
  virtual const std::string &getErrorMessageWhenPersonNotFound() = 0;

protected:
  std::list<const Person *> persons;
};

#endif // CONTAINER_HPP
