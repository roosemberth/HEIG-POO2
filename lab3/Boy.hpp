#ifndef BOY_HPP
#define BOY_HPP

#include "Person.hpp"

class Boy : public Person {
public:
  Boy(const std::string &name);

  const std::string &getName() const;

  bool canBeWith(const std::list<const Person *> &persons,
                 std::string &error) const;

  bool canDriveBoat() const;

private:
  const std::string name;
};

#endif