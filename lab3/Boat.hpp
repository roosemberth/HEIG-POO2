#ifndef BOAT_HPP
#define BOAT_HPP

#include "Container.hpp"

class Boat : public Container {
public:
  Boat();
  bool isLeft() const;
  void move();
  bool canMove(std::string &error);
  bool canAdd(const Person *p, std::string &error);

protected:
  const std::string &getErrorMessageWhenPersonNotFound();

private:
  bool isBoatLeft;
};

#endif // BOAT_HPP