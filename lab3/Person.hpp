#ifndef PERSON_HPP
#define PERSON_HPP

#include <list>
#include <string>

class Person {
public:
  virtual ~Person();

  /**
   * @brief Returns the name to be displayed to the user for this person.
   */
  virtual const std::string &getName() const = 0;

  /**
   * @brief Whether this person can drive the boat on their own.
   */
  virtual bool canDriveBoat() const = 0;

  /**
   * @brief Whether this person can stop thieves on their own.
   */
  virtual bool canStopThief() const;

  /**
   * @brief Whether this person is a mother.
   */
  virtual bool isMother() const;

  /**
   * @brief Whether this person is a father.
   */
  virtual bool isFather() const;

  /**
   * @brief Whether this person can be with everyone in the specified set.
   */
  virtual bool canBeWith(const std::list<const Person *> &persons,
                         std::string &error) const;
};

#endif // PERSON_HPP