#include <algorithm>

#include "Thief.hpp"

using namespace std;

const string thiefName = "voleur";

const string &Thief::getName() const { return thiefName; }

bool Thief::canBeWith(const std::list<const Person *> &persons,
                      std::string &error) const {
  std::list<const Person *>::const_iterator policeman =
      find_if(persons.begin(), persons.end(), [](const Person *const person) {
        return person->canStopThief();
      });

  if (policeman == persons.end()) {
    if (persons.size() > 1) {
      error =
          "### Le voleur ne peut pas etre seul avec des membres de la famille";
      return false;
    }
  }

  // On pourrais juste retourner true mais on retourne la methode parent pour
  // rester compatible en cas d'evolution
  return Person::canBeWith(persons, error);
}

bool Thief::canDriveBoat() const { return false; }
