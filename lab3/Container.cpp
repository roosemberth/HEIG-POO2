#include <algorithm>

#include "Container.hpp"

bool Container::canAdd(const Person *p, std::string &error) {
  persons.push_back(p);

  for (const Person *person : persons) {
    if (!person->canBeWith(persons, error)) {
      persons.remove(p);
      return false;
    }
  }

  persons.remove(p);
  return true;
}

bool Container::canRemove(const std::string &personName, const Person *&p,
                          std::string &error) {
  std::list<const Person *>::iterator deplacee = find_if(
      persons.begin(), persons.end(), [&personName](const Person *&person) {
        return person->getName() == personName;
      });

  if (deplacee == persons.end()) {
    error = getErrorMessageWhenPersonNotFound();
    return false;
  }

  p = *deplacee;

  persons.remove(p);

  for (const Person *person : persons) {
    if (!person->canBeWith(persons, error)) {
      persons.push_back(p);
      return false;
    }
  }

  persons.push_back(p);

  return true;
}

void Container::add(const Person *p) { persons.push_back(p); }

void Container::remove(const Person *p) { persons.remove(p); }

void Container::empty() {
  for (const Person *person : persons) {
    delete person;
  }
  persons.clear();
}

const std::list<const Person *> &Container::getPersons() const {
  return persons;
}
