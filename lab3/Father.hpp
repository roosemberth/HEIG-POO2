#ifndef FATHER_HPP
#define FATHER_HPP

#include "Person.hpp"

class Father : public Person {
public:
  const std::string &getName() const;

  bool isFather() const;

  bool canDriveBoat() const;
};

#endif