#ifndef THIEF_HPP
#define THIEF_HPP

#include "Person.hpp"

class Thief : public Person {
public:
  const std::string &getName() const;

  bool canBeWith(const std::list<const Person *> &persons,
                 std::string &error) const;

  bool canDriveBoat() const;
};

#endif