#include "Boat.hpp"

using namespace std;

const string message = "### La personne ne peut pas etre debarque car elle "
                       "n'est pas sur le bateau";

Boat::Boat() : isBoatLeft(true) {}

bool Boat::isLeft() const { return isBoatLeft; }

void Boat::move() { isBoatLeft = !isBoatLeft; }

bool Boat::canMove(string &error) {
  for (const Person *p : persons) {
    if (p->canDriveBoat()) {
      return true;
    }
  }
  error =
      "### Le bateau ne peut pas se deplacer car personne ne peut le diriger";
  return false;
}

bool Boat::canAdd(const Person *p, std::string &error) {
  if (persons.size() > 1) {
    error = "### Le bateau ne peut pas contenir plus de deux personnes";
    return false;
  }
  return Container::canAdd(p, error);
}

const string &Boat::getErrorMessageWhenPersonNotFound() { return message; }