#ifndef MOTHER_HPP
#define MOTHER_HPP

#include "Person.hpp"

class Mother : public Person {
public:
  const std::string &getName() const;

  bool isMother() const;

  bool canDriveBoat() const;
};

#endif