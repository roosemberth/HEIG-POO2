#include "Mother.hpp"

using namespace std;

const string motherName = "mere";

const string &Mother::getName() const { return motherName; }

bool Mother::isMother() const { return true; }

bool Mother::canDriveBoat() const { return true; }