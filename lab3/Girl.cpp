#include <algorithm>

#include "Girl.hpp"

Girl::Girl(const std::string &_name) : name(_name) {}

const std::string &Girl::getName() const { return name; }

bool Girl::canBeWith(const std::list<const Person *> &persons,
                     std::string &error) const {
  std::list<const Person *>::const_iterator father =
      find_if(persons.begin(), persons.end(),
              [](const Person *const person) { return person->isFather(); });

  if (father != persons.end()) {
    std::list<const Person *>::const_iterator mother =
        find_if(persons.begin(), persons.end(),
                [](const Person *const person) { return person->isMother(); });

    if (mother == persons.end()) {
      error = "### Une fille ne peut pas etre avec son pere sans sa mere";
      return false;
    }
  }

  // On pourrais juste retourner true mais on retourne la methode parent pour
  // rester compatible en cas d'evolution
  return Person::canBeWith(persons, error);
}

bool Girl::canDriveBoat() const { return false; }
