#include "Father.hpp"

using namespace std;

const string fatherName = "pere";

const string &Father::getName() const { return fatherName; }

bool Father::isFather() const { return true; }

bool Father::canDriveBoat() const { return true; }