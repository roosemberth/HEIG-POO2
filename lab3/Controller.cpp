#include "Controller.hpp"
#include "Boy.hpp"
#include "Container.hpp"
#include "Father.hpp"
#include "Girl.hpp"
#include "Mother.hpp"
#include "Policeman.hpp"
#include "Thief.hpp"

#include <iostream>

using namespace std;

Controller::Controller() : tour(0), endOfTheGame(false) { reinitialise(); }

Controller::~Controller() {
  right.empty();
  left.empty();
  boat.empty();
}

void Controller::showMenu() const {
  cout << "p       : afficher\n"
          "e <nom> : embarquer <nom>\n"
          "d <nom> : debarquer <nom>\n"
          "m       : deplacer bateau\n"
          "r       : reinitialiser\n"
          "q       : quitter\n"
          "h       : menu\n"
       << endl;
}

void Controller::display() const {
  cout << "----------------------------------------------------------\n"
          "Gauche :";
  left.getPersons();

  for (const Person *person : left.getPersons()) {
    cout << " " << person->getName();
  }

  cout << "\n"
          "----------------------------------------------------------\n";

  if (boat.isLeft()) {
    printBoat();
  } else {
    cout << "\n";
  }

  cout << "==========================================================\n";

  if (!boat.isLeft()) {
    printBoat();
  } else {
    cout << "\n";
  }

  cout << "----------------------------------------------------------\n"
          "Droite :";

  for (const Person *person : right.getPersons()) {
    cout << " " << person->getName();
  }

  cout << "\n"
          "----------------------------------------------------------\n"
       << endl;
}

void Controller::nextCommand() {
  string line;
  bool valide = false;
  while (!valide) {
    cout << tour << "> " << flush;
    getline(cin, line);
    if (line.length() == 1) {
      switch (line.at(0)) {
      case 'm':
        valide = moveBoat();
        break;
      case 'r':
        reinitialise();
        tour = -1;
        valide = true;
        break;
      case 'h':
        showMenu();
        break;
      case 'p':
        display();
        break;
      case 'q':
        endOfTheGame = true;
        valide = true;
        break;
      default:
        cout << "La commande entree est invalide, veuillez reessayer ou entrer "
                "h "
                "pour acceder au menu"
             << endl;
        break;
      }
    } else if (line.length() > 1) {
      switch (line.at(0)) {
      case 'e':
        valide = load(line.erase(0, 2));
        break;
      case 'd':
        valide = unload(line.erase(0, 2));
        break;
      default:
        cout << "La commande entree est invalide, veuillez reessayer ou entrer "
                "h "
                "pour acceder au menu"
             << endl;
        break;
      }
    }
  }
  tour++;
}

bool Controller::gameIsOver() const { return endOfTheGame; }

bool Controller::load(const std::string &personName) {
  Bank &current = boat.isLeft() ? left : right;
  string erreur;
  const Person *person = nullptr;
  if (current.canRemove(personName, person, erreur)) {
    if (boat.canAdd(person, erreur)) {
      current.remove(person);
      boat.add(person);
      return true;
    }
  }
  cout << erreur << endl;
  return false;
}

bool Controller::unload(const std::string &personName) {
  Bank &current = boat.isLeft() ? left : right;
  string erreur;
  const Person *person = nullptr;
  if (boat.canRemove(personName, person, erreur)) {
    if (current.canAdd(person, erreur)) {
      boat.remove(person);
      current.add(person);
      return true;
    }
  }
  cout << erreur << endl;
  return false;
}

bool Controller::moveBoat() {
  string erreur;
  if (boat.canMove(erreur)) {
    boat.move();
    return true;
  }
  cout << erreur << endl;
  return false;
}

void Controller::reinitialise() {
  right.empty();
  left.empty();
  boat.empty();
  left.add(new Father());
  left.add(new Mother());
  left.add(new Boy("paul"));
  left.add(new Boy("pierre"));
  left.add(new Girl("julie"));
  left.add(new Girl("jeanne"));
  left.add(new Policeman());
  left.add(new Thief());
  if (!boat.isLeft()) {
    boat.move();
  }
}

void Controller::printBoat() const {
  cout << "Bateau : <";
  for (const Person *person : boat.getPersons()) {
    cout << " " << person->getName();
  }
  cout << " >\n";
}