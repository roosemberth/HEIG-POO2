#include "Controller.hpp"

int main() {
  Controller game;
  game.showMenu();
  while (!game.gameIsOver()) {
    game.display();
    game.nextCommand();
  }
}