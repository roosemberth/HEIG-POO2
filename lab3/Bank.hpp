#ifndef BANK_HPP
#define BANK_HPP

#include "Container.hpp"

class Bank : public Container {
protected:
  const std::string &getErrorMessageWhenPersonNotFound();
};

#endif // BANK_HPP