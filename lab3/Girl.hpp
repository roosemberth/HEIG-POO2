#ifndef GIRL_HPP
#define GIRL_HPP

#include "Person.hpp"

class Girl : public Person {
public:
  Girl(const std::string &name);

  const std::string &getName() const;

  bool canBeWith(const std::list<const Person *> &persons,
                 std::string &error) const;

  bool canDriveBoat() const;

private:
  const std::string name;
};

#endif