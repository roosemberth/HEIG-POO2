#ifndef POLICEMAN_HPP
#define POLICEMAN_HPP

#include "Person.hpp"

class Policeman : public Person {
public:
  const std::string &getName() const;

  bool canDriveBoat() const;

  bool canStopThief() const;
};

#endif