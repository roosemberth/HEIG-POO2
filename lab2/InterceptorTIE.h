#ifndef INTERCEPTORTIE_H
#define INTERCEPTORTIE_H

#include "Ship.h"

class InterceptorTIE : public Ship {
public:
  InterceptorTIE();
  double getTotalWeight() const;
};

#endif // INTERCEPTORTIE_H
