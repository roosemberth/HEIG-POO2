#ifndef SHUTTLE_H
#define SHUTTLE_H

#include "CargoShip.h"

class Shuttle : public CargoShip {
public:
  Shuttle(double payload = 0);
};

#endif // SHUTTLE_H
