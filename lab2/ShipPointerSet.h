#include "Ship.h"
#ifndef Set_H
#define Set_H

class ShipPointerSetIterator;

/**
 * @brief Represent a set of Ship*.
 * A set is a collection where each element is unique
 */
class ShipPointerSet {
  friend ShipPointerSetIterator;

public:
  /**
   * @brief Create an empty ShipPointerSet
   */
  ShipPointerSet();

  ShipPointerSet(const ShipPointerSet &copy) = delete;
  ShipPointerSet &operator=(const ShipPointerSet &copy) = delete;
  ShipPointerSet(ShipPointerSet &&source);
  ShipPointerSet &operator=(ShipPointerSet &&source);

  /**
   * @brief Add an element at the beginning of the set
   * @param pointer The element to add
   */
  void addFirst(const Ship *pointer);

  /**
   * @brief Add an element at the end of the set
   * @param pointer The element to add
   */
  void addLast(const Ship *pointer);

  /**
   * @brief Remove from the set the element. If the element, is not in the set,
   * do nothing
   * @param pointer The element to remove
   */
  void remove(const Ship *pointer);

  /**
   * @brief Indicates if a set contains an element
   * @param pointer The element
   * @return True if the set contain the element
   */
  bool contain(const Ship *pointer) const;

  /**
   * @brief Give an iterator on the start of the set
   */
  ShipPointerSetIterator begin() const;

  /**
   * @brief Give an iterator after the end of the set
   */
  ShipPointerSetIterator end() const;

  ~ShipPointerSet();

private:
  /**
   * @brief An element of the set
   */
  class ShipPointerSetElem {
  public:
    /**
     * @brief Create an element of the list
     * @param value The value of the element
     * @param nextElem The next element
     */
    ShipPointerSetElem(const Ship *const value, ShipPointerSetElem *nextElem);

    /**
     * @brief Get the next element
     */
    ShipPointerSetElem *getNext() const;

    /**
     * @brief Set the next element
     */
    void setNext(ShipPointerSetElem *nextElem);

    /**
     * @brief Get the value of this element
     */
    const Ship *getValue() const;

    ~ShipPointerSetElem();

  private:
    /**
     * @brief The next element of the set
     */
    ShipPointerSetElem *next;

    /**
     * @brief The value of this element
     */
    const Ship *const value;
  };

  /**
   * @brief The first elem of the set
   */
  ShipPointerSetElem *first;
};

/**
 * @brief Represent an iterator for a ShipPointerSet
 */
class ShipPointerSetIterator {
  friend ShipPointerSet;

public:
  /**
   * @brief Test if two iterators point at the same element
   * @param other The iterator to compare
   * @return False if the two iterators point to the same element, True in the
   * other case
   */
  bool operator!=(const ShipPointerSetIterator &other);

  /**
   * @brief Increment the iterator to the next element
   */
  void operator++();

  /**
   * @brief Get the element pointed by the current iterator
   */
  const Ship *operator*();

private:
  /**
   * @brief Initialise the iterator with the first elem of the set
   * @param first the first elem of the set
   */
  ShipPointerSetIterator(const ShipPointerSet::ShipPointerSetElem *first);

  /**
   * @brief The current element
   */
  const ShipPointerSet::ShipPointerSetElem *current;
};

#endif // !Set_H