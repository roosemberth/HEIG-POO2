#include "StarDreadnought.h"

using namespace std;

static unsigned int shipCount = 0;

StarDreadnought::StarDreadnought(double _payload)
    : CargoShip("Super-class Star Destroyer", 9e9, 250e3, 40, ++shipCount) {
  setPayload(_payload);
}
