#include "InterceptorTIE.h"

using namespace std;

static unsigned int shipCount = 0;

InterceptorTIE::InterceptorTIE() : Ship("TIE / IN", 110, ++shipCount) {}

double InterceptorTIE::getTotalWeight() const { return 5; }
