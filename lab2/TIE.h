#ifndef TIE_H
#define TIE_H

#include "Ship.h"

class TIE : public Ship {
public:
  TIE();
  double getTotalWeight() const;
};

#endif // TIE_H
