#include "Shuttle.h"

using namespace std;

static unsigned int shipCount = 0;

Shuttle::Shuttle(double _payload)
    : CargoShip("Lambda - class shuttle", 360.0, 80.0, 54, ++shipCount) {
  setPayload(_payload);
}
