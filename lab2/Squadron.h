#ifndef SQUADRON_H
#define SQUADRON_H

#include <string>

#include "Ship.h"
#include "ShipPointerSet.h"

class Squadron;

std::ostream &operator<<(std::ostream &os, const Squadron &squadron);
Squadron operator+(const Squadron &squadron, const Ship *ship);
Squadron operator+(const Ship *ship, const Squadron &squadron);
Squadron operator-(const Squadron &squadron, const Ship *ship);

class Squadron {
public:
  Squadron(const Squadron &) = delete;
  Squadron &operator=(const Squadron &&) = delete;
  Squadron(Squadron &&);
  Squadron &operator=(Squadron &&);

  /**
   * @param name The name of the squadron.
   */
  Squadron(std::string name = "");

  /**
   * @brief Renames the squadron.
   *
   * @param name New name of the squadron.
   */
  void setName(std::string name);

  /**
   * @brief Get the name of the fleet.
   */
  std::string getName() const;

  /**
   * @brief Sets the leader of the squadron.
   *
   * If the leader is already part of the squadron, it will be promoted.
   * Any previous leader (if exists) is silently discarded.
   */
  void setLeader(const Ship *leader);

  /**
   * @brief Adds a ship to the squadron. @see Squadron::add;
   */
  void operator+=(const Ship *ship);

  /**
   * @brief Removes a ship from the squadron. @see Squadron::remove.
   */
  void operator-=(const Ship *ship);

  /**
   * @brief Adds a ship to the squadron if the ship is not part of it.
   *
   * Idempotent. If the provided ship is nullptr, does nothing.
   */
  void add(const Ship *ship);

  /**
   * @brief Removes the ship from the squadron if the ship is part of it.
   *
   * Idempotent. If the provided ship is nullptr, does nothing.
   */
  void remove(const Ship *ship);

  /**
   * @brief Returns a copy of the squadron where the specified ship is part of
   * the fleet.
   */
  Squadron withShip(const Ship *ship) const;

  /**
   * @brief Returns a copy of the squadron where the specified ship is not part
   * of the fleet.
   */
  Squadron withoutShip(const Ship *ship) const;

  /**
   * @brief Retrieves the total weight of the fleet, in tons.
   */
  double getTotalWeight() const;

  /**
   * @brief Calculates the total expected fuel of the fleet for this trip.
   *
   * @param distance Total distance of travel, in Megalight.
   * @param speed Cruise speed of travel, in Megalight per hour.
   *              If zero, the fuel will be calcultated using the max speed of
   *              the slowest ship.
   * @throws runtime_error If the requested speed if larger than the max speed
   *                       of the fleet.
   */
  double fuelUsage(unsigned int distance, unsigned int speed = 0) const;

  /**
   * @brief Calculate the max speed of the fleet.
   *
   * The max speed of the fleet is the minimum max speed.
   * If the fleet is empty, the max speed is zero.
   */
  unsigned int getMaxSpeed() const;

private:
  std::string name;
  const Ship *leader;
  ShipPointerSet ships;
  friend std::ostream &operator<<(std::ostream &os, const Squadron &);
};

#endif