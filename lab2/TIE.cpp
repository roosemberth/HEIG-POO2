#include "TIE.h"

using namespace std;

static unsigned int shipCount = 0;

TIE::TIE() : Ship("TIE / LN", 100, ++shipCount) {}

double TIE::getTotalWeight() const { return 6; }
