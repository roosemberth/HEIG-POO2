#include <sstream>

#include "CargoShip.h"

using namespace std;

CargoShip::CargoShip(std::string _model, double _emptyWeight,
                     double _maxPayload, unsigned int _maxSpeed,
                     unsigned int _id)
    : Ship(_model, _maxSpeed, _id), maxPayload(_maxPayload),
      emptyWeight(_emptyWeight), payload(0) {
  if (maxPayload < 0) {
    throw runtime_error("Max payload can not be negative");
  }

  if (emptyWeight < 0) {
    throw runtime_error("Empty weigt can not be negative");
  }
}

void CargoShip::setPayload(double _payload) {
  if (_payload < 0 || _payload > maxPayload) {
    throw runtime_error(
        "Payload must be greater than 0 and lesser than may payload");
  }
  payload = _payload;
}

double CargoShip::getPayload() const { return payload; }

double CargoShip::getTotalWeight() const { return payload + emptyWeight; }

ostream &CargoShip::toStream(ostream &os) const {
  // Cannot use std::fixed since modifies the stream configuration permanently.
  ostringstream maxPayloadSS;
  maxPayloadSS.precision(1);
  maxPayloadSS << fixed << maxPayload;

  return Ship::toStream(os) << "  cargo : " << getPayload() << " tons"
                            << " (max : " << maxPayloadSS.str() << ")" << endl;
}