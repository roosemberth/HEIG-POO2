#ifndef CARGOSHIP_H
#define CARGOSHIP_H

#include "Ship.h"

class CargoShip : public Ship {
protected:
  /**
   * @param model ship model.
   * @param emptyWeight Weight of the ship when it is empty, in tons.
   * @param maxPayload Max payload of the ship, in tons.
   * @param maxSpeed Max speed of the Ship, in Megalight per hour.
   * @param id Id of the Ship
   * @throw runtime_error If max payload or emptyWeight is lesser than 0, throw
   * runtime_error
   */
  CargoShip(std::string model, double emptyWeight, double maxPayload,
            unsigned int maxSpeed, unsigned int id);

public:
  /**
   * @brief Set the current payload of the ship, in tons.
   * @param payload New payload of the ship
   * @throw runtime_error If the new payload is greater than max payload, throw
   * runtime_error
   */
  void setPayload(double payload);

  /**
   * @brief Retrieves the total weight of the ship, in tons.
   */
  double getTotalWeight() const final;

  /**
   * @brief Return the current payload of the ship, in tons.
   */
  double getPayload() const;

  std::ostream &toStream(std::ostream &os) const;
  /**
   * @brief Max payload of the ship, in tons.
   */
  const double maxPayload;

  /**
   * @brief Weight of the ship when it is empty, in tons.
   */
  const double emptyWeight;

private:
  /**
   * @brief Current payload of the ship, in tons.
   */
  double payload;
};

#endif /* CARGOSHIP_H */
