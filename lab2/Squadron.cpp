#include <sstream>

#include "Squadron.h"

using namespace std;

Squadron::Squadron(Squadron &&src) : leader(src.leader) {
  name = move(src.name);
  ships = move(src.ships);
}

Squadron &Squadron::operator=(Squadron &&src) {
  name = move(src.name);
  leader = src.leader;
  ships = move(src.ships);
  return (*this);
}

Squadron::Squadron(string _name) : name(_name), leader(nullptr), ships() {}

void Squadron::setName(string _name) { name = _name; }

string Squadron::getName() const { return name; }

void Squadron::setLeader(const Ship *_leader) {
  ships.remove(_leader);
  leader = _leader;
}

void Squadron::operator+=(const Ship *ship) { add(ship); }

void Squadron::operator-=(const Ship *ship) { remove(ship); }

void Squadron::add(const Ship *newShip) {
  if (newShip != nullptr)
    ships.addLast(newShip);
}

void Squadron::remove(const Ship *ship) { ships.remove(ship); }

Squadron Squadron::withShip(const Ship *ship) const {
  auto newSquadron = Squadron(name);
  for (auto s : ships)
    newSquadron.add(s);
  newSquadron.add(ship);
  return newSquadron;
}

Squadron Squadron::withoutShip(const Ship *ship) const {
  auto newSquadron = Squadron(name);
  for (auto s : ships)
    if (s != ship)
      newSquadron.add(s);
  return newSquadron;
}

double Squadron::getTotalWeight() const {
  double weight = 0.0;
  if (leader != nullptr)
    weight += leader->getTotalWeight();
  for (auto s : ships)
    weight += s->getTotalWeight();
  return weight;
}

double Squadron::fuelUsage(unsigned int distance, unsigned int speed) const {
  auto maxSpeed = getMaxSpeed();
  if (speed == 0)
    speed = maxSpeed;

  if (speed > maxSpeed)
    throw runtime_error("The fleet cannot travel at the requested speed.");

  double fuelUsage = 0.0;
  if (leader != nullptr)
    fuelUsage += leader->getTotalTripFuel(distance, speed);

  for (auto s : ships)
    fuelUsage += s->getTotalTripFuel(distance, speed);

  return fuelUsage;
}

unsigned int Squadron::getMaxSpeed() const {
  unsigned int speed = 0;
  if (leader != nullptr)
    speed = leader->maxSpeed;
  for (auto s : ships)
    if (speed == 0 || s->maxSpeed < speed)
      speed = s->maxSpeed;
  return speed;
}

ostream &operator<<(ostream &os, const Squadron &squadron) {
  string name = squadron.getName();
  if (name.empty())
    name = "<Unnamed squadron>";
  // Cannot use std::fixed since modifies the stream configuration permanently.
  ostringstream weightSS;
  weightSS.precision(2);
  weightSS << fixed << squadron.getTotalWeight();

  os << "Squadron: " << name << endl
     << "  max speed : " << squadron.getMaxSpeed() << " MGLT" << endl
     << "  total weight : " << weightSS.str() << " tons" << endl
     << endl;

  if (squadron.leader != nullptr) {
    os << "-- Leader:" << endl;
    os << *squadron.leader << endl;
  }

  os << "-- Members:";
  for (auto s : squadron.ships)
    os << endl << *s;
  return os;
}

Squadron operator+(const Squadron &squadron, const Ship *ship) {
  return squadron.withShip(ship);
}

Squadron operator+(const Ship *ship, const Squadron &squadron) {
  return squadron + ship;
}

Squadron operator-(const Squadron &squadron, const Ship *ship) {
  return squadron.withoutShip(ship);
}