#include <cmath>
#include <sstream>

#include "Ship.h"

using namespace std;

Ship::Ship(string _model, unsigned int _maxSpeed, unsigned int _id)
    : model(_model), maxSpeed(_maxSpeed), id(_id), nickname("") {}

Ship::~Ship() {}

ostream &Ship::toStream(ostream &os) const {
  // Cannot use std::fixed since modifies the stream configuration permanently.
  ostringstream weightSS;
  weightSS.precision(2);
  weightSS << fixed << getTotalWeight();

  if (!nickname.empty())
    os << nickname << " ";

  os << "[" << model << " #" << id << "]" << endl;
  os << "  weight : " << weightSS.str() << " tons" << endl;
  os << "  max speed : " << maxSpeed << " MGLT" << endl;
  return os;
}

std::string Ship::toString() const {
  stringstream ss;
  toStream(ss);
  return ss.str();
}

void Ship::setNickname(string name) { nickname = name; }

const std::string &Ship::getNickname() const { return nickname; }

std::ostream &operator<<(std::ostream &os, const Ship &ship) {
  return ship.toStream(os);
}

double Ship::getTotalTripFuel(unsigned int distance, unsigned int speed) const {
  if (speed == 0)
    speed = maxSpeed;

  if (speed > maxSpeed)
    throw runtime_error("The ship cannot travel at the requested speed.");

  double weight = getTotalWeight();
  return cbrt(weight) * log10(weight * speed) * log10(distance + 1) / 2;
}