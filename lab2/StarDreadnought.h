#ifndef STARDREADNOUGHT_H
#define STARDREADNOUGHT_H

#include "CargoShip.h"

class StarDreadnought : public CargoShip {
public:
  StarDreadnought(double payload = 0);
};

#endif // SHUTTLE_H
