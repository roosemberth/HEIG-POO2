#ifndef SHIP_HPP
#define SHIP_HPP

#include <ostream>
#include <string>

class Ship;

std::ostream &operator<<(std::ostream &os, const Ship &ship);

/**
 * @brief Represents a ship.
 *
 * A ship has mass, a maximum travel speed and consumes fuel.
 */
class Ship {
protected:
  /**
   * @param model ship model.
   * @param maxSpeed Max speed of the Ship, in Megalight per hour.
   * @param id Id of the Ship
   */
  Ship(std::string model, unsigned int maxSpeed, unsigned int id);

public:
  virtual ~Ship();

  /**
   * @brief Prints the name of the ship into a stream.
   */
  virtual std::ostream &toStream(std::ostream &os) const;

  /**
   * @brief Return the name if the ship
   */
  std::string toString() const;

  /**
   * @brief Sets a friendly name for a ship.
   * Set an empty string to indicate that there is no friendly name
   */
  void setNickname(std::string name);

  /**
   * @brief Returns the friendly name of the ship.
   *
   * An empty string is returned if the ship has no friendly name.
   */
  const std::string &getNickname() const;

  /**
   * @brief Retrieves the total weight of the ship, in tons.
   */
  virtual double getTotalWeight() const = 0;

  /**
   * @brief Calculate the total fuel expected for this trip.
   *
   * @param distance Total distance of travel, in Megalight.
   * @param speed Cruise speed of travel, in Megalight per hour.
   *              If zero, the fuel will be calcultated using the max speed.
   * @throws runtime_error If the requested speed if larger than the max speed
   * of the ship.
   */
  virtual double getTotalTripFuel(unsigned int distance,
                                  unsigned int speed = 0) const;

  /**
   * @brief Model of the ship.
   */
  const std::string model;

  /**
   * @brief Max speed of the ship, in Megalight per hour.
   */
  const unsigned int maxSpeed;

  /**
   * @brief number uniquely associated to this instance of this ship model.
   */
  const unsigned int id;

private:
  /**
   * @brief Friendly name of the ship.
   *
   * If empty, the ship is considered to have no friendly name.
   */
  std::string nickname;
};

#endif /* SHIP_HPP */
