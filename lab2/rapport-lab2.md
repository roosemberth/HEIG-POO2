---
title: "Rapport du laboratoire 2: Escadrille"
lang: fr
author:
  - Florian Gazzetta
  - Roosemberth Palacios
---

## Schéma UML

```{.plantuml caption="Schéma UML" format="svg" width=480px}
@startuml
skinparam monochrome true
skinparam linetype polyline
skinparam linetype ortho
hide empty members
hide circle

class CargoShip {
  +emptyWeight : const double
  +maxPayload : const double
  +getTotalWeight() : double
  payload : double
  +toStream(std::ostream& os) : std::ostream&
}

class InterceptorTIE {
  +getTotalWeight() : double
}

abstract class Ship {
+model : const std::string
+id : const unsigned int
+maxSpeed : const unsigned int
+getTotalTripFuel(unsigned int distance, unsigned int speed) : double
+{abstract} getTotalWeight() : double
+toStream(std::ostream& os) : std::ostream&
nickname : std::string
}

class Shuttle {
  +Shuttle(double payload)
}

class Squadron {
  +Squadron(std::string name)
  -ships : ShipPointerSet
  +withShip(const Ship* ship) : Squadron
  +withoutShip(const Ship* ship) : Squadron
  -leader : const Ship*
  +fuelUsage(unsigned int distance, unsigned int speed) : double
  +getTotalWeight() : double
  name : std::string
  +getMaxSpeed() : unsigned int
  +add(const Ship* ship) : void
  +operator+=(const Ship* ship) : void
  +operator-=(const Ship* ship) : void
  +remove(const Ship* ship) : void
  +setLeader(const Ship* leader) : void
}

class StarDreadnought {
  +StarDreadnought(double payload)
}

class TIE {
  +getTotalWeight() : double
}

/' Inheritance relationships '/
.CargoShip <|-- .Shuttle
.CargoShip <|-- .StarDreadnought

.Ship <|-- .CargoShip
.Ship <|-- .InterceptorTIE
.Ship <|-- .TIE

/' Aggregation relationships '/
.ShipPointerSet o-- .Ship

.Squadron o-- .Ship
.Squadron --* .ShipPointerSet

@enduml
```

## Détails d'implémentation

### Utilisation d'un set ordonnée pour le stockage des vaisseaux au sein d'une escadrille

- On a utilisé un set afin d'éviter des doublons.
- Nous avons eu besoin de conserver l'ordre d'insertion pour s'aligner à
  la sortie d'exemple fournie.
- Nous avons utilisé une linked-list car c'était la solution la plus simple à
  implémenter.
- Pour éviter des failles d'encapsulation, nous avons effacé les constructeurs
  de copie de _Squadron_ et _ShipPointerSet_ et crée des constructeurs de
  déplacement explicites.

## Tests

- Nous avons fait des tests unitaires (voir le fichier `test.cpp`).
- Nous avons ajouté des tests qui valident notre implémentation contre les
  exemples fournis.
- Nous avons effectué des tests avec _Valgrind_ pour vérifier qu'il n'y a pas
  de fuites de mémoire.
