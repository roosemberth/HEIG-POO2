#include "InterceptorTIE.h"
#include "ShipPointerSet.h"
#include "Shuttle.h"
#include "Squadron.h"
#include "StarDreadnought.h"
#include "TIE.h"
#include "gtest/gtest.h"
#include <cmath>
#include <ostream>

using namespace std;

TEST(Set, AddFirst) {
  ShipPointerSet list;
  Ship *ship1 = new TIE();
  ship1->setNickname("Ship 1");
  Ship *ship2 = new TIE();
  ship2->setNickname("Ship 2");
  Ship *ship3 = new TIE();
  ship3->setNickname("Ship 3");
  Ship *ship4 = new TIE();
  ship4->setNickname("Ship 4");
  Ship *ship5 = new TIE();
  ship5->setNickname("Ship 5");
  Ship *ship6 = new TIE();
  ship6->setNickname("Ship 6");
  list.addFirst(ship1);
  list.addFirst(ship1);
  list.addFirst(ship2);
  list.addFirst(ship2);
  list.addFirst(ship3);
  list.addFirst(ship3);
  list.addFirst(ship4);
  list.addFirst(ship4);
  list.addFirst(ship5);
  list.addFirst(ship5);
  list.addFirst(ship1);
  list.addFirst(ship2);
  list.addFirst(ship3);
  list.addFirst(ship4);
  list.addFirst(ship5);
  list.remove(ship6);

  EXPECT_EQ(true, list.contain(ship1));
  EXPECT_EQ(true, list.contain(ship2));
  EXPECT_EQ(true, list.contain(ship3));
  EXPECT_EQ(true, list.contain(ship4));
  EXPECT_EQ(true, list.contain(ship5));
  EXPECT_EQ(false, list.contain(ship6));

  int valeur = 0;

  for (const Ship *ship : list) {
    valeur++;
  }

  EXPECT_EQ(5, valeur);
  list.remove(ship1);
  list.remove(ship1);
  list.remove(ship3);
  list.remove(ship3);
  list.remove(ship5);
  list.remove(ship5);

  EXPECT_EQ(false, list.contain(ship1));
  EXPECT_EQ(true, list.contain(ship2));
  EXPECT_EQ(false, list.contain(ship3));
  EXPECT_EQ(true, list.contain(ship4));
  EXPECT_EQ(false, list.contain(ship5));
  EXPECT_EQ(false, list.contain(ship6));

  for (const Ship *ship : list) {
    valeur++;
  }

  EXPECT_EQ(7, valeur);

  list.remove(ship4);
  list.remove(ship4);
  list.remove(ship2);
  list.remove(ship2);

  EXPECT_EQ(false, list.contain(ship1));
  EXPECT_EQ(false, list.contain(ship2));
  EXPECT_EQ(false, list.contain(ship3));
  EXPECT_EQ(false, list.contain(ship4));
  EXPECT_EQ(false, list.contain(ship5));
  EXPECT_EQ(false, list.contain(ship6));

  for (const Ship *ship : list) {
    valeur++;
  }

  EXPECT_EQ(7, valeur);

  list.addFirst(ship4);
  list.addFirst(ship6);

  EXPECT_EQ(false, list.contain(ship1));
  EXPECT_EQ(false, list.contain(ship2));
  EXPECT_EQ(false, list.contain(ship3));
  EXPECT_EQ(true, list.contain(ship4));
  EXPECT_EQ(false, list.contain(ship5));
  EXPECT_EQ(true, list.contain(ship6));

  delete ship1;
  delete ship2;
  delete ship3;
  delete ship4;
  delete ship5;
  delete ship6;
}

TEST(Set, AddLast) {
  ShipPointerSet list;
  Ship *ship1 = new TIE();
  ship1->setNickname("Ship 1");
  Ship *ship2 = new TIE();
  ship2->setNickname("Ship 2");
  Ship *ship3 = new TIE();
  ship3->setNickname("Ship 3");
  Ship *ship4 = new TIE();
  ship4->setNickname("Ship 4");
  Ship *ship5 = new TIE();
  ship5->setNickname("Ship 5");
  Ship *ship6 = new TIE();
  ship6->setNickname("Ship 6");
  list.addLast(ship1);
  list.addLast(ship1);
  list.addLast(ship2);
  list.addLast(ship2);
  list.addLast(ship3);
  list.addLast(ship3);
  list.addLast(ship4);
  list.addLast(ship4);
  list.addLast(ship5);
  list.addLast(ship5);
  list.addLast(ship1);
  list.addLast(ship2);
  list.addLast(ship3);
  list.addLast(ship4);
  list.addLast(ship5);
  list.remove(ship6);

  EXPECT_EQ(true, list.contain(ship1));
  EXPECT_EQ(true, list.contain(ship2));
  EXPECT_EQ(true, list.contain(ship3));
  EXPECT_EQ(true, list.contain(ship4));
  EXPECT_EQ(true, list.contain(ship5));
  EXPECT_EQ(false, list.contain(ship6));

  int valeur = 0;

  for (const Ship *ship : list) {
    valeur++;
  }

  EXPECT_EQ(5, valeur);
  list.remove(ship1);
  list.remove(ship1);
  list.remove(ship3);
  list.remove(ship3);
  list.remove(ship5);
  list.remove(ship5);

  EXPECT_EQ(false, list.contain(ship1));
  EXPECT_EQ(true, list.contain(ship2));
  EXPECT_EQ(false, list.contain(ship3));
  EXPECT_EQ(true, list.contain(ship4));
  EXPECT_EQ(false, list.contain(ship5));
  EXPECT_EQ(false, list.contain(ship6));

  for (const Ship *ship : list) {
    valeur++;
  }

  EXPECT_EQ(7, valeur);

  list.remove(ship4);
  list.remove(ship4);
  list.remove(ship2);
  list.remove(ship2);

  EXPECT_EQ(false, list.contain(ship1));
  EXPECT_EQ(false, list.contain(ship2));
  EXPECT_EQ(false, list.contain(ship3));
  EXPECT_EQ(false, list.contain(ship4));
  EXPECT_EQ(false, list.contain(ship5));
  EXPECT_EQ(false, list.contain(ship6));

  for (const Ship *ship : list) {
    valeur++;
  }

  EXPECT_EQ(7, valeur);

  list.addLast(ship4);
  list.addLast(ship6);

  EXPECT_EQ(false, list.contain(ship1));
  EXPECT_EQ(false, list.contain(ship2));
  EXPECT_EQ(false, list.contain(ship3));
  EXPECT_EQ(true, list.contain(ship4));
  EXPECT_EQ(false, list.contain(ship5));
  EXPECT_EQ(true, list.contain(ship6));

  delete ship1;
  delete ship2;
  delete ship3;
  delete ship4;
  delete ship5;
  delete ship6;
}

TEST(Squadron, Exemple) {
  Ship *blackLeader = new TIE();
  blackLeader->setNickname("Black leader");
  Ship *blackTwo = new TIE();
  Ship *shuttle = new Shuttle(23.4); // 23.4 tonnes de marchandises
  Squadron squad("Black Squadron");
  squad += blackLeader;
  squad += blackTwo;
  squad += shuttle;
  squad.setLeader(blackLeader);
  stringstream s;
  s << squad << endl;
  EXPECT_EQ(s.str(), "Squadron: Black Squadron\n"
                     "  max speed : 54 MGLT\n"
                     "  total weight : 395.40 tons\n"
                     "\n"
                     "-- Leader:\n"
                     "Black leader [TIE / LN #" +
                         std::to_string((*blackLeader).id) +
                         "]\n"
                         "  weight : 6.00 tons\n"
                         "  max speed : 100 MGLT\n"
                         "\n"
                         "-- Members:\n"
                         "[TIE / LN #" +
                         std::to_string((*blackTwo).id) +
                         "]\n"
                         "  weight : 6.00 tons\n"
                         "  max speed : 100 MGLT\n"
                         "\n"
                         "[Lambda - class shuttle #" +
                         std::to_string((*shuttle).id) +
                         "]\n"
                         "  weight : 383.40 tons\n"
                         "  max speed : 54 MGLT\n"
                         "  cargo : 23.4 tons (max : 80.0)\n"
                         "\n");
  delete blackLeader;
  delete blackTwo;
  delete shuttle;
}

TEST(Squadron, TotalWeight) {
  Squadron s;
  auto t1 = new TIE();
  auto t2 = new TIE();
  auto t3 = new TIE();
  auto t4 = new TIE();
  auto t5 = new TIE();
  s.add(t1);
  s.add(t2);
  s.add(t3);
  s.add(t4);
  s.add(t5);

  EXPECT_EQ(t1->getTotalWeight() * 5, s.getTotalWeight());

  delete t1;
  delete t2;
  delete t3;
  delete t4;
  delete t5;
}

TEST(Squadron, AddRemove) {
  Squadron s;
  Ship *tie1 = new TIE();
  Ship *tie2 = new TIE();
  Ship *tie3 = new TIE();

  EXPECT_EQ(s.getTotalWeight(), 0);
  s.add(nullptr);
  EXPECT_EQ(s.getTotalWeight(), 0);
  s.add(tie1);
  s.add(tie1);
  s.add(tie2);
  s.add(tie2);
  s.add(tie3);
  s.add(tie3);
  s.add(nullptr);
  s.add(tie1);
  s.add(tie2);
  s.add(tie3);
  EXPECT_EQ(s.getTotalWeight(), tie1->getTotalWeight() * 3);
  s.remove(nullptr);
  EXPECT_EQ(s.getTotalWeight(), tie1->getTotalWeight() * 3);
  s.remove(tie1);
  s.remove(tie1);
  s.remove(tie2);
  s.remove(tie2);
  s.remove(tie3);
  s.remove(tie3);
  EXPECT_EQ(s.getTotalWeight(), 0);

  delete tie1;
  delete tie2;
  delete tie3;
}

TEST(Squadron, OperateurAddRemove) {
  Squadron s;
  Ship *tie1 = new TIE();
  Ship *tie2 = new TIE();
  Ship *tie3 = new TIE();

  EXPECT_EQ(s.getTotalWeight(), 0);
  s += nullptr;
  EXPECT_EQ(s.getTotalWeight(), 0);
  s += tie1;
  s += tie1;
  s += tie2;
  s += tie2;
  s += tie3;
  s += tie3;
  s += nullptr;
  s += tie1;
  s += tie2;
  s += tie3;
  EXPECT_EQ(s.getTotalWeight(), tie1->getTotalWeight() * 3);
  s -= nullptr;
  EXPECT_EQ(s.getTotalWeight(), tie1->getTotalWeight() * 3);
  s -= tie1;
  s -= tie1;
  s -= tie2;
  s -= tie2;
  s -= tie3;
  s -= tie3;
  EXPECT_EQ(s.getTotalWeight(), 0);

  delete tie1;
  delete tie2;
  delete tie3;
}

TEST(Squadron, AdditionSoustraction) {
  Squadron s;
  Ship *tie1 = new TIE();
  Ship *tie2 = new TIE();
  Ship *tie3 = new TIE();

  EXPECT_EQ(s.getTotalWeight(), 0);
  s = s + nullptr;
  EXPECT_EQ(s.getTotalWeight(), 0);
  s = tie1 + s;
  s = tie1 + s;
  s = tie2 + s;
  s = tie2 + s;
  s = tie3 + s;
  s = tie3 + s;
  s = nullptr + s;
  s = tie1 + s;
  s = tie2 + s;
  s = tie3 + s;
  EXPECT_EQ(s.getTotalWeight(), tie1->getTotalWeight() * 3);
  s = s - nullptr;
  EXPECT_EQ(s.getTotalWeight(), tie1->getTotalWeight() * 3);
  s = s - tie1;
  s = s - tie1;
  s = s - tie2;
  s = s - tie2;
  s = s - tie3;
  s = s - tie3;
  EXPECT_EQ(s.getTotalWeight(), 0);

  delete tie1;
  delete tie2;
  delete tie3;
}

TEST(Squadron, Nom) {
  Squadron s;
  EXPECT_EQ(s.getName(), "");
  s.setName("test");
  EXPECT_EQ(s.getName(), "test");
  s.setName("");
  EXPECT_EQ(s.getName(), "");
}

TEST(Ships, ShipInStream) {
  stringstream s1;
  Ship *ship1 = new TIE();
  s1 << *ship1;
  EXPECT_EQ(s1.str(), "[TIE / LN #" + to_string((*ship1).id) +
                          "]\n"
                          "  weight : 6.00 tons\n"
                          "  max speed : 100 MGLT\n");
  stringstream s2;
  Ship *ship2 = new Shuttle(23.4);
  s2 << *ship2;
  EXPECT_EQ(s2.str(), "[Lambda - class shuttle #" + to_string((*ship2).id) +
                          "]\n"
                          "  weight : 383.40 tons\n"
                          "  max speed : 54 MGLT\n"
                          "  cargo : 23.4 tons (max : 80.0)\n");
  delete ship1;
  delete ship2;
}

TEST(Ships, ShipToStream) {
  stringstream s1;
  Ship *ship1 = new TIE();
  ship1->toStream(s1);
  EXPECT_EQ(s1.str(), "[TIE / LN #" + to_string((*ship1).id) +
                          "]\n"
                          "  weight : 6.00 tons\n"
                          "  max speed : 100 MGLT\n");
  stringstream s2;
  Ship *ship2 = new Shuttle(23.4);
  ship2->toStream(s2);
  EXPECT_EQ(s2.str(), "[Lambda - class shuttle #" + to_string((*ship2).id) +
                          "]\n"
                          "  weight : 383.40 tons\n"
                          "  max speed : 54 MGLT\n"
                          "  cargo : 23.4 tons (max : 80.0)\n");
  delete ship1;
  delete ship2;
}

TEST(Ships, ShipToString) {
  Ship *ship1 = new TIE();
  EXPECT_EQ(ship1->toString(), "[TIE / LN #" + to_string((*ship1).id) +
                                   "]\n"
                                   "  weight : 6.00 tons\n"
                                   "  max speed : 100 MGLT\n");
  Ship *ship2 = new Shuttle(23.4);
  EXPECT_EQ(ship2->toString(), "[Lambda - class shuttle #" +
                                   to_string((*ship2).id) +
                                   "]\n"
                                   "  weight : 383.40 tons\n"
                                   "  max speed : 54 MGLT\n"
                                   "  cargo : 23.4 tons (max : 80.0)\n");
  delete ship1;
  delete ship2;
}

TEST(Ships, Nickname) {
  Ship *ship = new TIE();

  EXPECT_EQ(ship->getNickname(), "");
  EXPECT_EQ(ship->toString(), "[TIE / LN #" + to_string((*ship).id) +
                                  "]\n"
                                  "  weight : 6.00 tons\n"
                                  "  max speed : 100 MGLT\n");

  ship->setNickname("Black Leader");

  EXPECT_EQ(ship->getNickname(), "Black Leader");
  EXPECT_EQ(ship->toString(), "Black Leader [TIE / LN #" +
                                  to_string((*ship).id) +
                                  "]\n"
                                  "  weight : 6.00 tons\n"
                                  "  max speed : 100 MGLT\n");

  ship->setNickname("");

  EXPECT_EQ(ship->getNickname(), "");
  EXPECT_EQ(ship->toString(), "[TIE / LN #" + to_string((*ship).id) +
                                  "]\n"
                                  "  weight : 6.00 tons\n"
                                  "  max speed : 100 MGLT\n");

  ship->setNickname("Blue Leader");

  EXPECT_EQ(ship->getNickname(), "Blue Leader");
  EXPECT_EQ(ship->toString(), "Blue Leader [TIE / LN #" +
                                  to_string((*ship).id) +
                                  "]\n"
                                  "  weight : 6.00 tons\n"
                                  "  max speed : 100 MGLT\n");
  delete ship;
}

TEST(Ships, Modele_Valides) {
  Ship *tie = new TIE();
  EXPECT_EQ((*tie).maxSpeed, 100);
  EXPECT_EQ((*tie).model, "TIE / LN");

  Ship *interceptorTie = new InterceptorTIE();
  EXPECT_EQ((*interceptorTie).maxSpeed, 110);
  EXPECT_EQ((*interceptorTie).model, "TIE / IN");

  Ship *starDreadnought = new StarDreadnought();
  EXPECT_EQ((*starDreadnought).maxSpeed, 40);
  EXPECT_EQ((*starDreadnought).model, "Super-class Star Destroyer");

  Ship *shuttle = new Shuttle();
  EXPECT_EQ((*shuttle).maxSpeed, 54);
  EXPECT_EQ((*shuttle).model, "Lambda - class shuttle");

  delete tie;
  delete interceptorTie;
  delete starDreadnought;
  delete shuttle;
}

TEST(Ships, ID_Valides) {
  Ship *tie1 = new TIE();
  Ship *tie2 = new TIE();
  Ship *tie3 = new TIE();
  Ship *interceptorTie1 = new InterceptorTIE();
  Ship *interceptorTie2 = new InterceptorTIE();
  Ship *interceptorTie3 = new InterceptorTIE();
  Ship *shuttle1 = new Shuttle();
  Ship *shuttle2 = new Shuttle();
  Ship *shuttle3 = new Shuttle();
  Ship *starDreadnought1 = new StarDreadnought();
  Ship *starDreadnought2 = new StarDreadnought();
  Ship *starDreadnought3 = new StarDreadnought();

  EXPECT_EQ((*tie2).id, (*tie1).id + 1);
  EXPECT_EQ((*tie3).id, (*tie1).id + 2);

  EXPECT_EQ((*interceptorTie2).id, (*interceptorTie1).id + 1);
  EXPECT_EQ((*interceptorTie3).id, (*interceptorTie1).id + 2);

  EXPECT_EQ((*shuttle2).id, (*shuttle1).id + 1);
  EXPECT_EQ((*shuttle3).id, (*shuttle1).id + 2);

  EXPECT_EQ((*starDreadnought2).id, (*starDreadnought1).id + 1);
  EXPECT_EQ((*starDreadnought3).id, (*starDreadnought1).id + 2);

  delete tie1;
  delete tie2;
  delete tie3;
  delete interceptorTie1;
  delete interceptorTie2;
  delete interceptorTie3;
  delete shuttle1;
  delete shuttle2;
  delete shuttle3;
  delete starDreadnought1;
  delete starDreadnought2;
  delete starDreadnought3;
}

TEST(Ships, Poids_Valides) {
  Ship *tie = new TIE();
  Ship *interceptorTie = new InterceptorTIE();
  CargoShip *cargoShuttle = new Shuttle();
  CargoShip *cargoStarDreadnought = new StarDreadnought();

  Ship *shuttle = cargoShuttle;
  Ship *starDreadnought = cargoStarDreadnought;

  EXPECT_EQ(tie->getTotalWeight(), 6);
  EXPECT_EQ(interceptorTie->getTotalWeight(), 5);
  EXPECT_EQ(shuttle->getTotalWeight(), 360);
  EXPECT_EQ(starDreadnought->getTotalWeight(), 9e9);

  EXPECT_EQ((*cargoShuttle).emptyWeight, 360);
  EXPECT_EQ((*cargoShuttle).maxPayload, 80);
  EXPECT_EQ((*cargoStarDreadnought).emptyWeight, 9e9);
  EXPECT_EQ((*cargoStarDreadnought).maxPayload, 250e3);

  EXPECT_EQ(cargoShuttle->getPayload(), 0);
  EXPECT_EQ(cargoStarDreadnought->getPayload(), 0);

  cargoShuttle->setPayload(50);
  cargoStarDreadnought->setPayload(150e3);

  EXPECT_EQ(cargoShuttle->getPayload(), 50);
  EXPECT_EQ(cargoStarDreadnought->getPayload(), 150e3);
  EXPECT_EQ(shuttle->getTotalWeight(), 410);
  EXPECT_EQ(starDreadnought->getTotalWeight(), 9e9 + 150e3);

  cargoShuttle->setPayload(0);
  cargoStarDreadnought->setPayload(0);

  EXPECT_EQ(cargoShuttle->getPayload(), 0);
  EXPECT_EQ(cargoStarDreadnought->getPayload(), 0);
  EXPECT_EQ(shuttle->getTotalWeight(), 360);
  EXPECT_EQ(starDreadnought->getTotalWeight(), 9e9);

  delete cargoShuttle;
  delete cargoStarDreadnought;
  cargoShuttle = new Shuttle(50);
  cargoStarDreadnought = new StarDreadnought(150e3);
  shuttle = cargoShuttle;
  starDreadnought = cargoStarDreadnought;

  cargoShuttle->setPayload(50);
  cargoStarDreadnought->setPayload(150e3);

  EXPECT_EQ(cargoShuttle->getPayload(), 50);
  EXPECT_EQ(cargoStarDreadnought->getPayload(), 150e3);
  EXPECT_EQ(shuttle->getTotalWeight(), 410);
  EXPECT_EQ(starDreadnought->getTotalWeight(), 9e9 + 150e3);
  EXPECT_THROW(cargoShuttle->setPayload(-1), runtime_error);
  EXPECT_THROW(cargoShuttle->setPayload(81), runtime_error);
  EXPECT_THROW(cargoStarDreadnought->setPayload(-1), runtime_error);
  EXPECT_THROW(cargoStarDreadnought->setPayload(251e3), runtime_error);

  shuttle = nullptr;
  starDreadnought = nullptr;

  delete tie;
  delete interceptorTie;
  delete cargoShuttle;
  delete cargoStarDreadnought;
}

TEST(Ships, Consomation_Carburant) {
  Ship *tie = new TIE();
  unsigned int distance = 10;

  EXPECT_DOUBLE_EQ(tie->getTotalTripFuel(distance),
                   cbrt(tie->getTotalWeight()) *
                       log10(tie->getTotalWeight() * tie->maxSpeed) *
                       log10(distance + 1) / 2);

  distance = 20;

  EXPECT_DOUBLE_EQ(tie->getTotalTripFuel(distance),
                   cbrt(tie->getTotalWeight()) *
                       log10(tie->getTotalWeight() * tie->maxSpeed) *
                       log10(distance + 1) / 2);

  EXPECT_DOUBLE_EQ(tie->getTotalTripFuel(0), 0);

  unsigned int vitesse = 20;
  distance = 10;

  EXPECT_DOUBLE_EQ(tie->getTotalTripFuel(distance, vitesse),
                   cbrt(tie->getTotalWeight()) *
                       log10(tie->getTotalWeight() * vitesse) *
                       log10(distance + 1) / 2);

  distance = 20;

  EXPECT_DOUBLE_EQ(tie->getTotalTripFuel(distance, vitesse),
                   cbrt(tie->getTotalWeight()) *
                       log10(tie->getTotalWeight() * vitesse) *
                       log10(distance + 1) / 2);

  EXPECT_DOUBLE_EQ(tie->getTotalTripFuel(0, vitesse), 0);

  EXPECT_THROW(tie->getTotalTripFuel(10, 1000), runtime_error);

  delete tie;
}

TEST(Ships, Consomation_Carburant_cargo) {
  CargoShip *shuttle = new Shuttle();
  unsigned int distance = 10;

  EXPECT_DOUBLE_EQ(shuttle->getTotalTripFuel(distance),
                   cbrt(shuttle->getTotalWeight()) *
                       log10(shuttle->getTotalWeight() * shuttle->maxSpeed) *
                       log10(distance + 1) / 2);

  distance = 20;

  EXPECT_DOUBLE_EQ(shuttle->getTotalTripFuel(distance),
                   cbrt(shuttle->getTotalWeight()) *
                       log10(shuttle->getTotalWeight() * shuttle->maxSpeed) *
                       log10(distance + 1) / 2);

  EXPECT_DOUBLE_EQ(shuttle->getTotalTripFuel(0), 0);

  unsigned int vitesse = 20;
  distance = 10;

  EXPECT_DOUBLE_EQ(shuttle->getTotalTripFuel(distance, vitesse),
                   cbrt(shuttle->getTotalWeight()) *
                       log10(shuttle->getTotalWeight() * vitesse) *
                       log10(distance + 1) / 2);

  distance = 20;

  EXPECT_DOUBLE_EQ(shuttle->getTotalTripFuel(distance, vitesse),
                   cbrt(shuttle->getTotalWeight()) *
                       log10(shuttle->getTotalWeight() * vitesse) *
                       log10(distance + 1) / 2);

  EXPECT_DOUBLE_EQ(shuttle->getTotalTripFuel(0, vitesse), 0);

  shuttle->setPayload(20);
  distance = 10;

  EXPECT_DOUBLE_EQ(shuttle->getTotalTripFuel(distance),
                   cbrt(shuttle->getTotalWeight()) *
                       log10(shuttle->getTotalWeight() * shuttle->maxSpeed) *
                       log10(distance + 1) / 2);

  distance = 20;

  EXPECT_DOUBLE_EQ(shuttle->getTotalTripFuel(distance),
                   cbrt(shuttle->getTotalWeight()) *
                       log10(shuttle->getTotalWeight() * shuttle->maxSpeed) *
                       log10(distance + 1) / 2);

  EXPECT_DOUBLE_EQ(shuttle->getTotalTripFuel(0), 0);

  vitesse = 20;
  distance = 10;

  EXPECT_DOUBLE_EQ(shuttle->getTotalTripFuel(distance, vitesse),
                   cbrt(shuttle->getTotalWeight()) *
                       log10(shuttle->getTotalWeight() * vitesse) *
                       log10(distance + 1) / 2);

  distance = 20;

  EXPECT_DOUBLE_EQ(shuttle->getTotalTripFuel(distance, vitesse),
                   cbrt(shuttle->getTotalWeight()) *
                       log10(shuttle->getTotalWeight() * vitesse) *
                       log10(distance + 1) / 2);

  EXPECT_DOUBLE_EQ(shuttle->getTotalTripFuel(0, vitesse), 0);

  EXPECT_THROW(shuttle->getTotalTripFuel(10, 1000), runtime_error);

  delete shuttle;
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}