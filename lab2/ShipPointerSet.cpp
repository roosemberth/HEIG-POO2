#include "ShipPointerSet.h"

ShipPointerSet::ShipPointerSet() : first(nullptr) {}

ShipPointerSet::ShipPointerSet(ShipPointerSet &&source) : first(source.first) {
  source.first = nullptr;
}

ShipPointerSet &ShipPointerSet::operator=(ShipPointerSet &&source) {
  delete first;
  first = source.first;
  source.first = nullptr;
  return (*this);
}

void ShipPointerSet::addFirst(const Ship *pointer) {
  if (!contain(pointer)) {
    first = new ShipPointerSet::ShipPointerSetElem(pointer, first);
  }
}

void ShipPointerSet::addLast(const Ship *pointer) {
  if (first == nullptr) {
    first = new ShipPointerSet::ShipPointerSetElem(pointer, nullptr);
  }

  ShipPointerSet::ShipPointerSetElem *i;
  for (i = first; i->getNext() != nullptr; i = i->getNext()) {
    if (i->getValue() == pointer) {
      return;
    }
  }

  if (i->getValue() == pointer) {
    return;
  }

  i->setNext(new ShipPointerSet::ShipPointerSetElem(pointer, nullptr));
}

void ShipPointerSet::remove(const Ship *pointer) {
  if (first == nullptr) {
    return;
  }

  if (first->getValue() == pointer) {
    ShipPointerSet::ShipPointerSetElem *temp = first;
    first = first->getNext();
    temp->setNext(nullptr);
    delete temp;
    return;
  }

  for (ShipPointerSet::ShipPointerSetElem *i = first; i->getNext() != nullptr;
       i = i->getNext()) {
    if (i->getNext()->getValue() == pointer) {
      ShipPointerSet::ShipPointerSetElem *temp = i->getNext()->getNext();
      i->getNext()->setNext(nullptr);
      delete i->getNext();
      i->setNext(temp);
      return;
    }
  }
}

bool ShipPointerSet::contain(const Ship *pointer) const {
  for (ShipPointerSet::ShipPointerSetElem *i = first; i != nullptr;
       i = i->getNext()) {
    if (i->getValue() == pointer) {
      return true;
    }
  }
  return false;
}

ShipPointerSetIterator ShipPointerSet::begin() const {
  return ShipPointerSetIterator(first);
}

ShipPointerSetIterator ShipPointerSet::end() const {
  return ShipPointerSetIterator(nullptr);
}

ShipPointerSet::~ShipPointerSet() { delete first; }

ShipPointerSetIterator::ShipPointerSetIterator(
    const ShipPointerSet::ShipPointerSetElem *first)
    : current(first) {}

bool ShipPointerSetIterator::operator!=(const ShipPointerSetIterator &other) {
  return other.current != current;
}

void ShipPointerSetIterator::operator++() { current = current->getNext(); }

const Ship *ShipPointerSetIterator::operator*() { return current->getValue(); }

ShipPointerSet::ShipPointerSetElem::ShipPointerSetElem(
    const Ship *value, ShipPointerSetElem *nextElem)
    : value(value), next(nextElem) {}

ShipPointerSet::ShipPointerSetElem *
ShipPointerSet::ShipPointerSetElem::getNext() const {
  return next;
}

void ShipPointerSet::ShipPointerSetElem::setNext(ShipPointerSetElem *nextElem) {
  next = nextElem;
}

const Ship *ShipPointerSet::ShipPointerSetElem::getValue() const {
  return value;
}

ShipPointerSet::ShipPointerSetElem::~ShipPointerSetElem() { delete next; }