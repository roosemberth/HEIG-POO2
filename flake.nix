# This is a Nix Flake. This file describes the necessary environment needed to
# build the projects. Nix is not required to build any of them, as long as the
# necessary dependencies are available in the system.
# This file is provided merely for convenience.
# See https://nixos.wiki/wiki/Flakes for more information about Nix flakes.
{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-20.09";

  outputs = { self, nixpkgs }: let
    systems = [ "x86_64-linux" "i686-linux" ];
    forAllSystems = f: nixpkgs.lib.genAttrs systems (system: f (import nixpkgs {
      inherit system; overlays = [ self.overlay ];
    }));
  in {
    overlay = final: prev: with final; let
      packageForLab = name: src: lab-env.overrideAttrs(_:{
          inherit name src;
          installPhase = ''
            mkdir -p "$out/bin"
            ${pkgs.findutils}/bin/find . -executable -type f \
              -exec cp '{}' "$out/bin/" \;
          '';
        });
    in {
      lab-env = stdenv.mkDerivation {
        name = "POO-lab-env";
        nativeBuildInputs = [ gnumake gcc gtest python3Packages.gcovr ];
        allowSubstitutes = false;
        preferLocalBuild = true;
      };
      lab1 = packageForLab "lab1" ./lab1;
      lab2 = packageForLab "lab2" ./lab2;
      lab3 = packageForLab "lab3" ./lab3;
    };

    devShell = forAllSystems (pkgs: pkgs.lab-env);

    # Export all packages from the overlay attribute.
    packages = forAllSystems (pkgs: with pkgs.lib;
      getAttrs (attrNames (self.overlay {} {})) pkgs
    );
  };
}
