#include <iostream>
#include <string>
#include "Matrix.hpp"
#include "Operations.hpp"

using namespace std;

int main(int argc, char** argv) {
	if (argc != 6)
	{
		cout << "Le programme attends de recevoir 5 arguments" << endl;
		return 0;
	}

	int m1Rows = stoi(argv[1]);
	int m1Column = stoi(argv[2]);
	int m2Rows = stoi(argv[3]);
	int m2Column = stoi(argv[4]);
	int modulus = stoi(argv[5]);

	if (m1Rows < 1 || m1Column < 1 || m2Rows < 1 || m2Column < 1 || modulus < 1)
	{
		cout << "Une des valeurs passee en argument est inferieure a 0" << endl;
		return 0;
	}

	cout << "The modulus is " << modulus << endl << endl << "one" << endl;
	Matrix m1(m1Rows, m1Column, modulus);
	cout << m1 << endl << "two" << endl;
	Matrix m2(m2Rows, m2Column, modulus);
	cout << m2 << endl;
	cout << "one + two" << endl << Matrix::apply(Addition::getInstance(), m1, m2) << endl;
	cout << "one - two" << endl << Matrix::apply(Substraction::getInstance(), m1, m2) << endl;
	cout << "one * two" << endl << Matrix::apply(Multiplication::getInstance(), m1, m2) << endl;

	return 0;
}