#ifndef OPERATION_HPP
#define OPERATION_HPP

/**
 * Abstract type embodying a closed binary operation over signed ints.
 */
class Operation {
public:
  /**
   * @brief Applies a binary closed operation over signed ints.
   */
  virtual int apply(int, int) const = 0;
};

#endif // OPERATION_HPP
