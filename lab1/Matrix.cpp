#include <limits>
#include <ostream>
#include <cstdlib>
#include <ctime>

#include "Matrix.hpp"
#include "Operations.hpp"

unsigned int **allocate_matrix(size_t rows, size_t cols) {
  unsigned int **ret = new unsigned int *[rows];
  for (size_t i = 0; i < rows; ++i)
    ret[i] = new unsigned int[cols];
  return ret;
}

void free_matrix(unsigned int **matrix, size_t rows) {
  for (size_t i = 0; i < rows; ++i)
    delete[] matrix[i];
  delete[] matrix;
}

Matrix::Matrix(size_t rows, size_t cols, unsigned int modulo,
               unsigned int constant)
    : rows(rows), cols(cols), modulo(modulo) {
  // We use runtime_errors here per explicit handout instructions.
  if (modulo == 0)
    throw std::runtime_error("The modulo cannot be zero.");
  if (modulo > (unsigned int)std::numeric_limits<int>::max())
    throw std::runtime_error("Modulo too big.");
  storage = allocate_matrix(rows, cols);
  unsigned int constantAfterMod = constant % modulo;
  for (size_t i = 0; i < rows; i++)
    for (size_t j = 0; j < cols; j++)
      storage[i][j] = constantAfterMod;
}

Matrix::Matrix(size_t rows, size_t cols, unsigned int modulo)
    : Matrix(rows, cols, modulo, 0) {
  srand((unsigned int)time(NULL));
  for (size_t i = 0; i < rows; i++)
    for (size_t j = 0; j < cols; j++)
      storage[i][j] = (unsigned int)(rand() / (RAND_MAX + 1.0) * modulo);
}

Matrix::Matrix(const Matrix &source)
    : Matrix(source.rows, source.cols, source.modulo, 0) {
  for (size_t i = 0; i < rows; i++)
    for (size_t j = 0; j < cols; j++)
      storage[i][j] = source.storage[i][j];
}

/**
 * @Brief throws an exception if the specified index is out of the Matrix
 * bounds.
 */
void Matrix::indexGuard(size_t row, size_t col) const {
  if (row >= rows || col >= cols || rows < 0 || cols < 0)
    throw std::runtime_error("Location out of boundaries.");
}

unsigned int Matrix::at(size_t row, size_t col) const {
  indexGuard(row, col);
  return storage[row][col];
}

void Matrix::set(size_t row, size_t col, int value) {
	indexGuard(row, col);

	if (value >= 0)
		storage[row][col] = value % modulo;
	else { // Map to the generator element of its equivalence class.
	  // Cast the modulo since only the signed modulo operation is well defined
	  // in C++11 (ISO 14882:2011§5.7), thus portable.
		storage[row][col] = value % (int)modulo + modulo;
	}
}

unsigned int Matrix::getMod() const { return modulo; }

Matrix::~Matrix() { free_matrix(storage, rows); }

/**
 * @brief Ensures the other matrix is smaller and of the same modulo.
 *
 * @raises std::invalid_argument if the modulos disagree.
 * @raises std::runtime_error if the other matrix is bigger.
 */
void Matrix::compatibleMatricesGuard(const Matrix &other) const {
  if (rows < other.rows || cols < other.cols)
    throw std::runtime_error("Destination matrix is too small.");
  if (modulo != other.modulo)
    throw std::invalid_argument("Modulos of matrices mismatch.");
}

void Matrix::apply(const Operation &op, const Matrix &other) {
  compatibleMatricesGuard(other);
  Matrix &self = *this;
  size_t nOverlappingRows = std::min(other.rows, rows);
  size_t nOverlappingCols = std::min(other.cols, cols);

  /**
   * To perform an efficient traversal of the matrix, it is decomposed in three
   * sections where Q1 corresponds to the area overlapped by the other matrix.
   *
   * +------+----+
   * |      |    |
   * |  Q1  | Q2 |
   * |      |    |
   * +------+----+
   * |    Q3     |
   * +------+----+
   */

  // Q1
  for (size_t i = 0; i < nOverlappingRows; i++)
    for (size_t j = 0; j < nOverlappingCols; j++)
			self.set(i, j, op.apply(self.at(i, j), other.at(i, j)));

  // Q2
  for (size_t i = 0; i < nOverlappingRows; i++)
    for (size_t j = nOverlappingCols; j < std::max(other.cols, cols); j++)
			self.set(i, j, op.apply(self.at(i, j), 0));

  // Q3
  for (size_t i = nOverlappingRows; i < std::max(other.rows, rows); i++)
    for (size_t j = 0; j < std::max(other.cols, cols); j++)
			self.set(i, j, op.apply(self.at(i, j), 0));
}

Matrix Matrix::apply(const Operation &op, const Matrix &m1, const Matrix &m2) {
  size_t rows = std::max(m1.rows, m2.rows), cols = std::max(m1.cols, m2.cols);
  Matrix res(rows, cols, m1.modulo, 0);
  res.apply(Addition::getInstance(), m1);
  res.apply(op, m2);
  return res;
}

Matrix *Matrix::pApply(const Operation &op, const Matrix &m1,
                       const Matrix &m2) {
  size_t rows = std::max(m1.rows, m2.rows), cols = std::max(m1.cols, m2.cols);
  Matrix *res = new Matrix(rows, cols, m1.modulo, 0);
  res->apply(Addition::getInstance(), m1);
  res->apply(op, m2);
  return res;
}

void Matrix::operator=(const Matrix& other)
{
	if (rows == other.rows && cols == other.cols)
	{
		if (modulo == other.modulo)
		{
			for (size_t i = 0; i < rows; i++)
				for (size_t j = 0; j < cols; j++)
					storage[i][j] = other.storage[i][j];
		}
		else
		{
			modulo = other.modulo;
			for (size_t i = 0; i < rows; i++)
				for (size_t j = 0; j < cols; j++)
					storage[i][j] = other.storage[i][j] % modulo;
		}
	}
	else
	{
		free_matrix(storage, rows);
		rows = other.rows;
		cols = other.cols;
		modulo = other.modulo;
		storage = allocate_matrix(rows, cols);
		for (size_t i = 0; i < rows; i++)
			for (size_t j = 0; j < cols; j++)
				storage[i][j] = other.storage[i][j];
	}
}

bool Matrix::operator==(const Matrix &other) const {
  if (rows != other.rows || cols != other.cols)
    return false;
  if (modulo != other.modulo)
    return false;
  for (size_t i = 0; i < rows; i++)
    for (size_t j = 0; j < cols; j++)
      if (at(i, j) != other.at(i, j))
        return false;
  return true;
}

std::ostream &operator<<(std::ostream &o, const Matrix &ref) {
  for (size_t i = 0; i < ref.rows; i++) {
    for (size_t j = 0; j < ref.cols; j++)
      o << ref.at(i, j) << " ";
    o << std::endl;
  };
  return o;
}
