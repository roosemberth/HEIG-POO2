#include <gtest/gtest.h>

#include "Matrix.hpp"
#include "Operations.hpp"

using namespace std;

#define add Addition::getInstance()
#define sub Substraction::getInstance()
#define mul Multiplication::getInstance()

TEST(Matrix, Creation_avec_constante_egale_a_modulo) {
  const unsigned int rows = 10, cols = 10, mod = 10;

  Matrix underTest(rows, cols, mod, mod);

  for (size_t x = 0; x < rows; ++x)
    for (size_t y = 0; y < cols; ++y)
      // Initializing a matrix with a value equal to the mod should initialise
      // its values to zero.
      EXPECT_EQ(underTest.at(x, y), 0u)
          << "Erroneous value detected at (" << x << "," << y << ")";
}

TEST(Matrix, Creation_avec_constante_superieure_a_modulo) {
  const unsigned int rows = 10, cols = 10, mod = 10, value = 17;

  Matrix underTest(rows, cols, mod, value);

  for (size_t x = 0; x < rows; ++x)
    for (size_t y = 0; y < cols; ++y)
      EXPECT_EQ(underTest.at(x, y), 7u)
          << "Erroneous value detected at (" << x << "," << y << ")";
}

TEST(Matrix, Creation_avec_constante_inferieure_a_modulo) {
  const unsigned int rows = 10, cols = 10, mod = 10, value = 7;

  Matrix underTest(rows, cols, mod, value);

  for (size_t x = 0; x < rows; ++x)
    for (size_t y = 0; y < cols; ++y)
      EXPECT_EQ(underTest.at(x, y), 7u)
          << "Erroneous value detected at (" << x << "," << y << ")";
}

TEST(Matrix, Creation_avec_valeur_aleatoire) {
  const unsigned int rows = 10, cols = 10, mod = 10;

  Matrix underTest(rows, cols, mod);

  for (size_t x = 0; x < rows; ++x)
    for (size_t y = 0; y < cols; ++y)
      EXPECT_TRUE(underTest.at(x, y) < mod)
          << "Erroneous value detected at (" << x << "," << y << ")";
}

TEST(Matrix, ElementAccess) {
  const unsigned int rows = 10, cols = 10, mod = 10;
  Matrix underTest(rows, cols, mod);

  for (size_t row = 0; row < rows; ++row) {
    for (size_t col = 0; col < cols; ++col) {
			underTest.set(row, col, (unsigned int)row);
			EXPECT_EQ(underTest.at(row, col), (unsigned int)row);
    }
  }

  EXPECT_EQ(underTest, underTest);
}

TEST(Matrix, PrintingMatrix) {
  const unsigned int size = 10;
  Matrix underTest(size, size, 10, 0);

  for (size_t x = 0; x < size; ++x)
    for (size_t y = 0; y < size; ++y) {
			underTest.set(x, y, (unsigned int)y);
      EXPECT_EQ(underTest.at(x, y), (unsigned int)y);
    }

  stringstream buffer;
  buffer << underTest;

  size_t nLines = 0;
  string line;
  while (getline(buffer, line)) {
    EXPECT_STREQ(line.c_str(), "0 1 2 3 4 5 6 7 8 9 ");
    ++nLines;
  }
  EXPECT_EQ(nLines, 10);
}

TEST(Matrix, Operations) {
  const unsigned int rows = 10, cols = 10, mod = 5;
  Matrix zeroMatrix(rows, cols, mod, 0);

  Matrix randomMatrix1(rows, cols, mod);

  Matrix m1(randomMatrix1);
  m1.apply(sub, m1);

  EXPECT_EQ(m1, zeroMatrix);

  EXPECT_EQ(m1, Matrix::apply(sub, m1, m1));
  Matrix *mp = Matrix::pApply(sub, m1, m1);
  EXPECT_EQ(m1, *mp);
  delete mp;
}

TEST(Matrix, SumMinus) {
  Matrix m1(3, 4, 5);
  Matrix m2(3, 4, 5);

  Matrix a1 = Matrix::apply(add, m1, m2);
  Matrix s2 = Matrix::apply(sub, m1, m2);

  // (m1 + m2) + (m1 - m2) = 2 * m1
  EXPECT_EQ(Matrix::apply(add, m1, m1), Matrix::apply(add, a1, s2));
}

TEST(Matrix, SumMod) {
  Matrix m1(5, 5, 5);
  Matrix copy(m1);

  for (size_t i = 0; i < m1.getMod(); ++i)
    copy.apply(add, m1);
  EXPECT_EQ(m1, copy);
}

TEST(Matrix, SubMod) {
  Matrix m1(5, 5, 10);
  Matrix copy(m1);

  for (size_t i = 0; i < m1.getMod(); ++i)
    copy.apply(sub, m1);
  EXPECT_EQ(m1, copy);
}

TEST(Matrix, FermatsLittleThm) {
  Matrix m1(4, 5, 5);
  Matrix copy(m1);

  EXPECT_EQ(m1, copy);

  // m1 = m1**(p-1) (mod p) -- Fermat's little theorem
  for (size_t i = 0; i < m1.getMod() - 1; ++i)
    copy.apply(mul, m1);

  EXPECT_EQ(m1, copy);
}

TEST(Matrix, SumMatricesDifferentSize) {
  Matrix m1(4, 5, 5, 1);
  Matrix m2(5, 4, 5, 1);

  Matrix m12 = Matrix::apply(add, m1, m2);
  Matrix r1(m12);
  for (int i = 0; i < 9; ++i)
    r1.apply(add, m12);

  EXPECT_EQ(Matrix(5, 5, 5, 0), r1);

  Matrix n1(5, 4, 5, 1);
  Matrix n2(4, 5, 5, 1);

  Matrix n12 = Matrix::apply(add, n1, n2);
  Matrix r2(n12);
  for (int i = 0; i < 9; ++i)
    r2.apply(add, n12);

  EXPECT_EQ(Matrix(5, 5, 5, 0), r1);

  Matrix o1(5, 4, 100, 1);
	Matrix o2(4, 5, 100, 1);
	Matrix o12 = Matrix::apply(add, o1, o2);
	Matrix r3(o12);
	for (int i = 0; i < 3; ++i)
		r3.apply(add, o12);

	for (size_t i = 0; i < 4; i++)
	{
		for (size_t j = 0; j < 4; j++)
		{
			EXPECT_EQ(r3.at(i, j), 8);
		}
	}

	for (size_t i = 0; i < 4; i++)
	{
		EXPECT_EQ(r3.at(i, 4), 4);
		EXPECT_EQ(r3.at(4, i), 4);
	}

	EXPECT_EQ(r3.at(4, 4), 0);
}

TEST(Matrix, ExceptionIsRisenOnIncompatibleDestination) {
  Matrix m1(4, 5, 5, 1);
  Matrix m2(5, 4, 5, 1);
  EXPECT_THROW(m1.apply(add, m2), std::runtime_error);
}

TEST(Matrix, SetAt) {
	Matrix m1(2, 3, 5, 1);
	m1.set(0, 0, 4);
	m1.set(1, 1, 8);
	EXPECT_EQ(m1.at(0, 0), 4);
	EXPECT_EQ(m1.at(1, 1), 3);
	EXPECT_THROW(m1.set(3, 3, 2), std::runtime_error);
}

TEST(Matrix, Copie) {
	unsigned int modulo = 5;
	Matrix m1(7, 6, modulo, 1);
	for (int i = 0; i < 7; i++)
	{
		for (int j = 0; j < 6; j++)
			m1.set(i, j, i + j);
	}

	Matrix m2(m1);
	for (int i = 0; i < 7; i++)
	{
		for (int j = 0; j < 6; j++)
			EXPECT_EQ(m2.at(i, j), (i + j) % modulo);
	}

	EXPECT_EQ(m2.getMod(), modulo);
	EXPECT_THROW(m2.at(7, 0), std::runtime_error);
	EXPECT_THROW(m2.at(0, 6), std::runtime_error);
	EXPECT_THROW(m2.at(7, 6), std::runtime_error);
}

TEST(Matrix, OperateurEgalite) {
	Matrix m1(3, 7, 10, 1);
	Matrix m2(4, 6, 11, 1);
	Matrix m3(5, 5, 12, 1);

	m1.set(1, 1, 4);
	m2.set(1, 2, 5);
	m3.set(1, 3, 6);
	m2 = m1;
	m3 = m2;
	m1 = m3;
	m1.set(2, 1, 7);
	m2.set(2, 2, 8);
	m3.set(2, 3, 9);

	EXPECT_EQ(m1.getMod(), 10);
	EXPECT_EQ(m2.getMod(), 10);
	EXPECT_EQ(m3.getMod(), 10);

	EXPECT_EQ(m1.at(1, 1), 4);
	EXPECT_EQ(m2.at(1, 1), 4);
	EXPECT_EQ(m3.at(1, 1), 4);
	EXPECT_EQ(m1.at(1, 2), 1);
	EXPECT_EQ(m2.at(1, 2), 1);
	EXPECT_EQ(m3.at(1, 2), 1);
	EXPECT_EQ(m1.at(1, 3), 1);
	EXPECT_EQ(m2.at(1, 3), 1);
	EXPECT_EQ(m3.at(1, 3), 1);

	EXPECT_EQ(m1.at(2, 1), 7);
	EXPECT_EQ(m2.at(2, 1), 1);
	EXPECT_EQ(m3.at(2, 1), 1);
	EXPECT_EQ(m1.at(2, 2), 1);
	EXPECT_EQ(m2.at(2, 2), 8);
	EXPECT_EQ(m3.at(2, 2), 1);
	EXPECT_EQ(m1.at(2, 3), 1);
	EXPECT_EQ(m2.at(2, 3), 1);
	EXPECT_EQ(m3.at(2, 3), 9);

	EXPECT_EQ(m1.at(2, 6), 1);
	EXPECT_EQ(m2.at(2, 6), 1);
	EXPECT_EQ(m3.at(2, 6), 1);

	EXPECT_THROW(m1.at(4, 4), std::runtime_error);
	EXPECT_THROW(m2.at(4, 4), std::runtime_error);
	EXPECT_THROW(m3.at(4, 4), std::runtime_error);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
