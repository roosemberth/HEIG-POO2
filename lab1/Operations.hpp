#ifndef OPERATIONS_HPP
#define OPERATIONS_HPP

#include "Operation.hpp"

class Addition : public Operation {
private:
  Addition();
  static Addition instance;

public:
	Addition(const Addition&) = delete;
	void operator=(const Addition&) = delete;
  virtual int apply(int, int) const;
  static const Addition &getInstance();
};

class Substraction : public Operation {
private:
  Substraction();
  static Substraction instance;

public:
	Substraction(const Substraction&) = delete;
	void operator=(const Substraction&) = delete;
  virtual int apply(int, int) const;
  static const Substraction &getInstance();
};

class Multiplication : public Operation {
private:
  Multiplication();
  static Multiplication instance;

public:
	Multiplication(const Multiplication&) = delete;
	void operator=(const Multiplication&) = delete;
  virtual int apply(int, int) const;
  static const Multiplication &getInstance();
};

#endif // OPERATIONS_HPP
