#!/usr/bin/env make

CC = g++

CC_FLAGS = -c -fPIC -pthread -O0 -g3
ifdef DEBUG
	CC_FLAGS += -DDEBUG
endif

# Flags for non-provided files #########
ifdef STRICT
	CC_EXTRA_FLAGS = -Werror
endif

CC_EXTRA_FLAGS += -W -Wall -Wextra -pedantic -Wcast-align -Wcast-qual -Wconversion
CC_EXTRA_FLAGS += -Wwrite-strings -Wfloat-equal -Wpointer-arith -Wformat=2 -Winit-self
CC_EXTRA_FLAGS += -Wuninitialized -Wshadow -Wmissing-declarations
CC_EXTRA_FLAGS += -Wno-unused-parameter -Wunreachable-code

LD_FLAGS = -Wl,--as-needed
LD_LIBS = -lgtest

# Target configuration #################
test-Matrix_SRCS = test-Matrix.cpp Matrix.cpp Operations.cpp

TESTS = test-Matrix
BINS  =

# Target profiles ######################
.SUFFIXES: # Disable default targets
.PHONY: all bins tests run-tests clean

all: bins tests

.ONESHELL: # Run all the rule in a single shell, but make will only expand the first line.
run-tests: bins tests
	@BINS="$(BINS)"; TESTS="$(TESTS)"
	@echo Running tests.
	@for test in ${TESTS}; do
	@	./$${test} || (
	@		echo Test ${test} Failed.
	@		exit 1
	@	)
	@done || exit 1
	@echo "Tests succedded, congratulations"

clean: scavenge
	rm -fr $(TESTS) $(BINS)

scavenge:
	rm -fr *.o

# Automatic targets ####################
bins: $(BINS)
tests: $(TESTS)

define TARGET_template =
$(1):: $$($(1)_SRCS:.cpp=.o)
	$$(CC) $$($(1)_LD_FLAGS) $$(filter %.o, $$^) $$(LD_FLAGS) $$(LD_LIBS) $$($(1)_LD_LIBS) -o $$@
endef

$(foreach out,$(TESTS),$(eval $(call TARGET_template,$(out))))
$(foreach out,$(BINS),$(eval $(call TARGET_template,$(out))))

ifdef STRICT
%.o: %.cpp-linted
	$(CC) $(CC_FLAGS) $(CC_EXTRA_FLAGS) $(<:-linted=) -o $@
else
%.o: %.cpp
	$(CC) $(CC_FLAGS) $(CC_EXTRA_FLAGS) $< -o $@
endif

%.cpp-linted:: %.cpp
	clang-format -i $<

verify-provided-files:
	sha256sum -c "Provided files sha256 checksums.txt"
