# Rapport de projet
Auteur : Florian Gazzetta, Roosemberth Palacios
## Détail d'implémentation

### Utilisation des types int et unsigned int
Dans notre code nous utilisons a la fois des int et des unsigned int pour les valeurs : par exemple le set prends un int en paramètre alors que le at retourne un unsigned int. En réalité, les valeurs que nous prenons en entrée sont des int pour gérer les cas qui impliquent un nombre négatif avec le modulo et pas un débordement de la capacitée. Par contre, les valeurs que l’on retourne en sortie sont des unsigned int car nous savons que toutes les valeurs contenues dans la matrice ne sont pas négative.

### Representation des operations par des classes
Pour toutes les opérations qu'il est possible d'effectuer dans les matrices, on a choisi de les représenter par une classe abstraite opération, dont dérivent toutes les classes qui représentent des opération. Si un utilisateur veut rajouter une opération, il doit donc juste créer une nouvelle classe qui hérite de opération.

### Utilisation du pattern singleton
Pour les classes qui représentent les opérations des matrices, nous avons choisis d'utiliser un pattern singleton afin de s'assurer qu'il n'y aurait toujours qu'une seule instance de chaque classe

## Tests 
Je ne sais pas quoi mettre la, on verra ensuite ce que l'on veut mettre.