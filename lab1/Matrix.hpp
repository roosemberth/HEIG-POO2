#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <ostream>
#include <vector>

#include "Operation.hpp"

/**
 * Matrix of arbitrary size with positive elements of a finite field.
 *
 * Fields are encoded by inscribing an integer modulo.
 * Elements are represented by using unsigned ints.
 * The modulo MUST be less of equal to INT_MAX.
 */
class Matrix {
private:
  size_t rows, cols;
  unsigned int **storage;
  unsigned int modulo;

public:
  /**
   * @brief Initialises a matrix with random values.
   */
  Matrix(size_t rows, size_t cols, unsigned int modulo);

  /**
   * @brief Initialises a matrix with a constant value.
   */
  Matrix(size_t rows, size_t cols, unsigned int modulo, unsigned int constant);

  Matrix(const Matrix &source);

  ~Matrix();

  void operator=(const Matrix&);

  bool operator==(const Matrix &) const;

  /**
   * @brief Accesses an element of the matrix.
   *
   * This method is provided as a 'const' alternative to the explicit conversion
   * of Matrix::Element.
   */
  unsigned int at(size_t row, size_t col) const;

	/**
	 * @brief Set an element of the matrix
	*/
	void set(size_t row, size_t col, int value);

  /**
   * @brief Retrieves the mod of a matrix.
   */
  unsigned int getMod() const;

  /**
   * @brief Applies an element-wise binary operation in-place.
   *
   * The operation is applied in-place.
   * Both matrices MUST have the same modulo.
   * The second matrix MUST be of smaller or equal dimension, missing elements
   * will be replaced with zero.
   * See @ref Matrix::apply for combining matrices of arbitrary size.
   */
  void apply(const Operation &, const Matrix &other);

  /**
   * @brief Applies an element-wise binary op over matrices of arbitrary size.
   */
  static Matrix apply(const Operation &, const Matrix &, const Matrix &);

  /**
   * @brief Returns a Matrix dynamically allocated. See @ref apply.
   */
  static Matrix *pApply(const Operation &, const Matrix &, const Matrix &);

private:
  friend std::ostream &operator<<(std::ostream &os, const Matrix &p);
  void indexGuard(size_t row, size_t col) const;
  void compatibleMatricesGuard(const Matrix &) const;
};

#endif // !MATRIX_HPP
