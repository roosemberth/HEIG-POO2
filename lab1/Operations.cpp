#include "Operations.hpp"

Addition Addition::instance;

Addition::Addition(){};

int Addition::apply(int a, int b) const {
  return a + b;
}

const Addition &Addition::getInstance() {
  return instance;
}

Substraction Substraction::instance;

Substraction::Substraction(){};

int Substraction::apply(int a, int b) const {
  return a - b;
}

const Substraction &Substraction::getInstance() {
  return instance;
}

Multiplication Multiplication::instance;

Multiplication::Multiplication(){};

int Multiplication::apply(int a, int b) const {
  return a * b;
}

const Multiplication &Multiplication::getInstance() {
  return instance;
}
