#ifndef FIELD_H
#define FIELD_H

#include <list>
#include <cstdlib>

//Pour eviter les dependence circulaire
class Humanoid;

class Field
{
private:
	std::list<Humanoid*> humanoids;
	unsigned int turn = 0;
	const std::size_t height;
	const std::size_t width;

public:
	Field(unsigned int humanCount, unsigned int vampireCount, std::size_t height, std::size_t width);
	int nextTurn();
	std::size_t getHeight() const;
	std::size_t getWidth() const;
	const Humanoid* humanoidAt(std::size_t posX, std::size_t posY) const;
	Humanoid* findNearest(Humanoid* source, bool (*function)(Humanoid*)) const;
	virtual ~Field();
	void addHumanoid(Humanoid* humanoid);
	bool hasVampire();
	bool hasHuman();
};

#endif // !FIELD_H