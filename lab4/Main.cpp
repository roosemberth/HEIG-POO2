#include <iostream>
#include <string>
#include <thread>

#include "Humanoids/Buffy.h"
#include "Field.h"
#include "Actions/Action.h"

using namespace std;

// Supprimer cette ligne si l'on souhaite que la simulation s'effectue sur une seul thread
#define MULTITHREAD

#ifdef MULTITHREAD

struct ThreadParam
{
	ThreadParam(unsigned int humanCount, unsigned int vampireCount, std::size_t heigth, std::size_t width) :
		humanCount(humanCount), vampireCount(vampireCount), heigth(heigth), width(width)
	{
	}

	unsigned int humanCount;
	unsigned int vampireCount;
	std::size_t heigth;
	std::size_t width;
	unsigned int buffyVictory = 0;
};

static void ThreadStatistics(ThreadParam* param)
{
	for (size_t i = 0; i < 2500; i++)
	{
		Field field(param->humanCount, param->vampireCount, param->heigth, param->width);
		while (field.hasVampire())
		{
			field.nextTurn();
		}

		if (field.hasHuman())
			param->buffyVictory++;
	}
}

void Statistics(unsigned int humanCount, unsigned int vampireCount, std::size_t heigth, std::size_t width)
{
	ThreadParam threadParam1(humanCount, vampireCount, heigth, width);
	ThreadParam threadParam2(humanCount, vampireCount, heigth, width);
	ThreadParam threadParam3(humanCount, vampireCount, heigth, width);
	ThreadParam threadParam4(humanCount, vampireCount, heigth, width);
	std::thread thread1(ThreadStatistics, &threadParam1);
	std::thread thread2(ThreadStatistics, &threadParam2);
	std::thread thread3(ThreadStatistics, &threadParam3);

	ThreadStatistics(&threadParam4);

	thread1.join();
	thread2.join();
	thread3.join();
	unsigned int buffyVictory = threadParam1.buffyVictory + threadParam2.buffyVictory + threadParam3.buffyVictory + threadParam4.buffyVictory;
	cout << "Buffy win " << buffyVictory << " times (" << buffyVictory / 100 << "%)" << endl;
}

#else

void Statistics(unsigned int humanCount, unsigned int vampireCount, std::size_t heigth, std::size_t width)
{
	unsigned int buffyVictory = 0;

	for (size_t i = 0; i < 10000; i++)
	{
		Field field(humanCount, vampireCount, heigth, width);
		while (field.hasVampire())
		{
			field.nextTurn();
		}

		if (field.hasHuman())
			buffyVictory++;
	}

	cout << "Buffy win " << buffyVictory << " times (" << buffyVictory / 100 << "%)" << endl;
}

#endif // MULTITHREAD

string FindCharacter(const Field& field, size_t posX, size_t posY)
{
	const Humanoid* humanoid = field.humanoidAt(posX, posY);

	if (humanoid == nullptr)
		return " ";

	return humanoid->getPrinting();
}

void printField(const Field& field)
{
	cout << '+';
	for (size_t i = 0; i < field.getWidth(); i++)
	{
		cout << '-';
	}
	cout << '+' << endl;

	for (size_t i = 0; i < field.getHeight(); i++)
	{
		cout << '|';
		for (size_t j = 0; j < field.getWidth(); j++)
		{
			cout << FindCharacter(field, j, i);
		}
		cout << '|' << endl;
	}

	cout << '+';
	for (size_t i = 0; i < field.getWidth(); i++)
	{
		cout << '-';
	}
	cout << '+' << endl;
}

int main(int argc, char* argv[])
{
	size_t heigth;
	size_t width;
	if (argc == 4)
	{
		int size = std::stoi(argv[3]);
		if (size < 0)
		{
			cout << "The size must be greater than zero";
			return EXIT_FAILURE;
		}
		heigth = size;
		width = size;
	}
	else if (argc == 5)
	{
		int size = std::stoi(argv[3]);
		if (size < 0)
		{
			cout << "The size must be greater than zero";
			return EXIT_FAILURE;
		}
		heigth = size;
		size = std::stoi(argv[4]);
		if (size < 0)
		{
			cout << "The size must be greater than zero";
			return EXIT_FAILURE;
		}
		width = size;
	}
	else
	{
		cout << "The program must receive three or four arguments.The first argument is "
			"the number of vampires. The second argument is the number of humans. The others indicate the size." << endl;
		return EXIT_FAILURE;
	}

	int count = std::atoi(argv[1]);
	if (count <= 0)
	{
		cout << "The number of humans must be greater than zero" << endl;
		return EXIT_FAILURE;
	}

	const unsigned int humanCount = count;
	count = std::atoi(argv[2]);
	if (count <= 0)
	{
		cout << "The number of vampires must be greater than zero" << endl;
		return EXIT_FAILURE;
	}
	const unsigned int vampireCount = count;

	srand((unsigned int)time(NULL));

	Field field(humanCount, vampireCount, heigth, width);

	string command;
	bool commandValid = false;
	while (true)
	{
		printField(field);
		int currentTurn = field.nextTurn();
		do
		{
			cout << "[" << currentTurn << "] q>uit s>tatistics n>ext : ";
			getline(cin, command);
			if (command == "" || command == "n")
			{
				commandValid = true;
			}
			else if (command == "q")
			{
				return EXIT_SUCCESS;
			}
			else if (command == "s")
			{
				Statistics(humanCount, vampireCount, heigth, width);
				commandValid = false;
			}
			else
			{
				commandValid = false;
			}
		} while (!commandValid);
	}
}