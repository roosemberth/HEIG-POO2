#ifndef BUFFY_H
#define BUFFY_H

#include "Human.h"

class Buffy : public Human
{
protected:
	virtual Action* selectAction(const Field& field);
public:
	Buffy(std::size_t posX, std::size_t posY);
	virtual std::string getPrinting() const;
	virtual bool isNormalHuman() const;
};

#endif // !BUFFY_H