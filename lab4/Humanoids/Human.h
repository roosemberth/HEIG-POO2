#ifndef HUMAN_H
#define HUMAN_H

#include "Humanoid.h"

class Human : public Humanoid
{
protected:
	virtual Action* selectAction(const Field& field);
public:
	Human(std::size_t posX, std::size_t posY);
	virtual std::string getPrinting() const;
	virtual bool isNormalHuman() const;
	virtual bool isVampire() const;
};

#endif // !HUMAN_H