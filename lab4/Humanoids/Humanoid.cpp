#include "Humanoid.h"

#include "../Actions/Action.h"
#include "../Field.h"
#include "../Actions/MoveAction.h"
#include "../Actions/StayAction.h"

using namespace std;

Humanoid::Humanoid(std::size_t posX, std::size_t posY) : posX(posX), posY(posY)
{
	nextAction = new StayAction();
}

Action* Humanoid::hunt(Humanoid* target, size_t speed)
{
	//Par circularit�, cette m�thode permet d'obtenir la valeur absolue du decalage en X et en Y
	size_t decalageX = min(target->getPosX() - getPosX(), getPosX() - target->getPosX());
	size_t decalageY = min(target->getPosY() - getPosY(), getPosY() - target->getPosY());

	if (decalageX + decalageY <= speed)
	{
		return new MoveAction(this, target->getPosX(), target->getPosY());
	}

	if (decalageX > decalageY)
	{
		if (target->getPosX() > getPosX())
		{
			return new MoveAction(this, getPosX() + speed, getPosY());
		}
		else
		{
			return new MoveAction(this, getPosX() - speed, getPosY());
		}
	}
	else
	{
		if (target->getPosY() > getPosY())
		{
			return new MoveAction(this, getPosX(), getPosY() + speed);
		}
		else
		{
			return new MoveAction(this, getPosX(), getPosY() - speed);
		}
	}

	return nullptr;
}

size_t Humanoid::distanceTo(Humanoid* target)
{
	// Cette m�thode fonctionne grace a la cicularit� des types non sign�s
	return  min(target->getPosX() - getPosX(), getPosX() - target->getPosX()) +
		min(target->getPosY() - getPosY(), getPosY() - target->getPosY());
}

void Humanoid::setAction(const Field& field)
{
	delete nextAction;
	nextAction = selectAction(field);
}

void Humanoid::executeAction(Field& field) const
{
	nextAction->execute(field);
}

bool Humanoid::isAlive() const
{
	return alive;
}

void Humanoid::moveTo(std::size_t posX, std::size_t posY)
{
	this->posX = posX;
	this->posY = posY;
}

void Humanoid::kill()
{
	alive = false;
}

std::size_t Humanoid::getPosX() const
{
	return posX;
}

std::size_t Humanoid::getPosY() const
{
	return posY;
}

Humanoid::~Humanoid()
{
	delete nextAction;
}