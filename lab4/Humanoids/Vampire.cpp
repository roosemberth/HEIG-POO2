#include "Vampire.h"

#include "../Field.h"
#include "../Actions/MoveAction.h"
#include "../Actions/VampireAttackAction.h"
#include "../Actions/StayAction.h"

using namespace std;

static bool isTargetPrey(Humanoid* target)
{
	return target->isNormalHuman();
}

Action* Vampire::selectAction(const Field& field)
{
	Humanoid* target = field.findNearest(this, isTargetPrey);

	if (target == nullptr)
	{
		return new StayAction();
	}

	if (distanceTo(target) < 2)
	{
		return new VampireAttackAction(target);
	}

	return hunt(target, 1);
}

Vampire::Vampire(std::size_t posX, std::size_t posY) : Humanoid(posX, posY)
{
}

string Vampire::getPrinting() const
{
#ifdef USE_COLOR
	return "\x1B[34mv\033[0m";
#else
	return "v";
#endif // USE_COLOR
}

bool Vampire::isNormalHuman() const
{
	return false;
}

bool Vampire::isVampire() const
{
	return true;
}