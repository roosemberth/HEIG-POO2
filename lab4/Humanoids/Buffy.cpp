#include "Buffy.h"

#include "../Field.h"
#include "../Actions/BuffyAttackAction.h"

using namespace std;

static bool isTargetVampire(Humanoid* target)
{
	return target->isVampire();
}

Action* Buffy::selectAction(const Field& field)
{
	Humanoid* target = field.findNearest(this, isTargetVampire);

	if (target == nullptr)
	{
		return Human::selectAction(field);
	}

	if (distanceTo(target) < 2)
	{
		return new BuffyAttackAction(target);
	}

	return hunt(target, 2);
}

Buffy::Buffy(std::size_t posX, std::size_t posY) : Human(posX, posY)
{
}

string Buffy::getPrinting() const
{
#ifdef USE_COLOR
	return "\x1B[33mB\033[0m";
#else
	return "B";
#endif // USE_COLOR
}

bool Buffy::isNormalHuman() const
{
	return false;
}