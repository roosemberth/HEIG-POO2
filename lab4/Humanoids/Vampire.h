#ifndef VAMPIRE_H
#define VAMPIRE_H

#include "Humanoid.h"

class Vampire : public Humanoid
{
protected:
	virtual Action* selectAction(const Field& field);
public:
	Vampire(std::size_t posX, std::size_t posY);
	virtual std::string getPrinting() const;
	virtual bool isNormalHuman() const;
	virtual bool isVampire() const;
};

#endif // !VAMPIRE_H