#include "Human.h"

#include "../Actions/MoveAction.h"
#include "../Field.h"

using namespace std;

static size_t modifyValue(size_t value, size_t max)
{
	if (value == 0)
	{
		return 1;
	}
	else if (value == max)
	{
		return	max - 1;
	}

	return value - 1 + ((rand() % 2) * 2);
}

Action* Human::selectAction(const Field& field)
{
	size_t newX = getPosX();
	size_t newY = getPosY();

	if (rand() % 2)
	{
		newX = modifyValue(newX, field.getWidth() - 1);
	}
	else
	{
		newY = modifyValue(newY, field.getHeight() - 1);
	}

	return new MoveAction(this, newX, newY);
}

Human::Human(std::size_t posX, std::size_t posY) : Humanoid(posX, posY)
{
}

string Human::getPrinting() const
{
#ifdef USE_COLOR
	return "\x1B[35mh\033[0m";
#else
	return "h";
#endif // USE_COLOR
}

bool Human::isNormalHuman() const
{
	return true;
}

bool Human::isVampire() const
{
	return false;
}