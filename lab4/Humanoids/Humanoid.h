#ifndef HUMANOID_H
#define HUMANOID_H

#define USE_COLOR // Si le terminal n'affiche pas correctement, supprimer cette ligne

#include <string>

//Pour eviter les dependence circulaire entre humanoid et field
class Field;
class Action;

class Humanoid
{
private:
	Action* nextAction = nullptr;
	bool alive = true;
	std::size_t posX;
	std::size_t posY;

protected:
	Humanoid(std::size_t posX, std::size_t posY);
	virtual Action* selectAction(const Field& field) = 0;
	Action* hunt(Humanoid* target, size_t speed);
	size_t distanceTo(Humanoid* target);

public:
	void setAction(const Field& field);
	void executeAction(Field& field) const;
	bool isAlive() const;
	void moveTo(std::size_t posX, std::size_t posY);
	virtual bool isNormalHuman() const = 0;
	virtual bool isVampire() const = 0;
	void kill();
	std::size_t getPosX() const;
	std::size_t getPosY() const;
	virtual std::string getPrinting() const = 0;
	virtual ~Humanoid();
};

#endif // !HUMANOID_H