#include "BuffyAttackAction.h"

BuffyAttackAction::BuffyAttackAction(Humanoid* target):target(target)
{
}

void BuffyAttackAction::execute(Field& field) const
{
	target->kill();
}