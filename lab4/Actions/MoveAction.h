#ifndef MOVEACTION_H
#define MOVEACTION_H

#include "Action.h"
#include "../Humanoids/Humanoid.h"

class MoveAction : public Action
{
private:
	Humanoid* const owner;
	const std::size_t posX;
	const std::size_t posY;

public:
	MoveAction(Humanoid* owner, std::size_t posX, std::size_t posY);
	virtual void execute(Field& field) const;
};

#endif // !MOVEACTION_H