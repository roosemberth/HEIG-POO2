#ifndef STAYACTION_H
#define STAYACTION_H

#include "Action.h"

class StayAction : public Action
{
public:
	virtual void execute(Field& field) const;
};

#endif