#include "VampireAttackAction.h"

#include "../Field.h"
#include "../Humanoids/Vampire.h"

VampireAttackAction::VampireAttackAction(Humanoid* target) : target(target)
{
}

void VampireAttackAction::execute(Field& field) const
{
	target->kill();

	if (rand() % 2)
	{
		field.addHumanoid(new Vampire(target->getPosX(), target->getPosY()));
	}
}