#include "MoveAction.h"

MoveAction::MoveAction(Humanoid* owner, std::size_t posX, std::size_t posY) :owner(owner), posX(posX), posY(posY)
{
}

void MoveAction::execute(Field& field) const
{
	owner->moveTo(posX,posY);
}