#ifndef ACTION_H
#define ACTION_H

class Field;

class Action
{
public:
	virtual void execute(Field& field) const = 0;
};

#endif // !ACTION_H