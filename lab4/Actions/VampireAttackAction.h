#ifndef VAMPIREATTACKACTION_H
#define VAMPIREATTACKACTION_H

#include "Action.h"
#include "../Humanoids/Humanoid.h"

class VampireAttackAction : public Action
{
private:
	Humanoid* target;
public:
	VampireAttackAction(Humanoid* target);
	virtual void execute(Field& field) const;
};

#endif // !VAMPIREATTACKACTION_H