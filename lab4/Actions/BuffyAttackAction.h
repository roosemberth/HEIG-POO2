#ifndef BUFFYATTACKACTION_H
#define BUFFYATTACKACTION_H

#include "../Humanoids/Humanoid.h"
#include "Action.h"

class BuffyAttackAction : public Action
{
private :
	Humanoid* target;

public:
	BuffyAttackAction(Humanoid* target);
	virtual void execute(Field& field) const;
};

#endif // !BUFFYATTACKACTION_H