#include "Field.h"

#include <list>

#include "Humanoids/Humanoid.h"
#include "Humanoids/Human.h"
#include "Humanoids/Buffy.h"
#include "Humanoids/Vampire.h"

using namespace std;

Field::Field(unsigned int humanCount, unsigned int vampireCount, std::size_t height, std::size_t width) :
	height(height), width(width)
{
	humanoids.push_back(new Buffy(rand() % width, rand() % height));

	for (size_t i = 0; i < vampireCount; i++)
	{
		humanoids.push_back(new Vampire(rand() % width, rand() % height));
	}

	for (size_t i = 0; i < humanCount; i++)
	{
		humanoids.push_back(new Human(rand() % width, rand() % height));
	}
}

int Field::nextTurn()
{
	// D�terminer les prochaines actions
	for (list<Humanoid*>::iterator it = humanoids.begin(); it != humanoids.end(); it++)
		(*it)->setAction(*this);
	// Executer les actions
	for (list<Humanoid*>::iterator it = humanoids.begin(); it != humanoids.end(); it++)
		(*it)->executeAction(*this);
	// Enlever les humanoides tu�s
	for (list<Humanoid*>::iterator it = humanoids.begin(); it != humanoids.end(); )
		if (!(*it)->isAlive()) {
			Humanoid* toDelete = *it;
			it = humanoids.erase(it); // suppression de l��l�ment dans la liste
			delete toDelete; // destruction de l�humanoide r�f�renc�
		}
		else
			++it;

	return turn++;
}

size_t Field::getHeight() const
{
	return height;
}

size_t Field::getWidth() const
{
	return width;
}

const Humanoid* Field::humanoidAt(std::size_t posX, std::size_t posY) const
{
	for (Humanoid* humanoid : humanoids)
	{
		if (humanoid->getPosX() == posX && humanoid->getPosY() == posY)
		{
			return humanoid;
		}
	}

	return nullptr;
}

Humanoid* Field::findNearest(Humanoid* source, bool(*function)(Humanoid*)) const
{
	Humanoid* selected = nullptr;
	size_t distance = numeric_limits<size_t>::max();

	for (Humanoid* humanoid : humanoids)
	{
		if (function(humanoid))
		{
			size_t newDistance;

			if (humanoid->getPosX() > source->getPosX())
			{
				newDistance = humanoid->getPosX() - source->getPosX();
			}
			else
			{
				newDistance = source->getPosX() - humanoid->getPosX();
			}

			if (humanoid->getPosY() > source->getPosY())
			{
				newDistance += humanoid->getPosY() - source->getPosY();
			}
			else
			{
				newDistance += source->getPosY() - humanoid->getPosY();
			}

			if (distance > newDistance)
			{
				distance = newDistance;
				selected = humanoid;
			}
		}
	}

	return selected;
}

Field::~Field()
{
	for (Humanoid* humanoid : humanoids)
	{
		delete humanoid;
	}
}

void Field::addHumanoid(Humanoid* humanoid)
{
	humanoids.push_back(humanoid);
}

bool Field::hasVampire()
{
	for (Humanoid* humanoid : humanoids)
	{
		if (humanoid->isVampire())
		{
			return true;
		}
	}
	return false;
}

bool Field::hasHuman()
{
	for (Humanoid* humanoid : humanoids)
	{
		if (humanoid->isNormalHuman())
		{
			return true;
		}
	}
	return false;
}